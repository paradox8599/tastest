import React from "react";
import { useRouter } from "next/navigation";
import { usePath } from "./usePath";
import { AppPaths } from "@/route";
import { appState } from "@/state";

/*
 * redirect to prepare page if media devices has not been set before tests
 */
export function usePrepare() {
  const path = usePath();
  const router = useRouter();

  React.useEffect(() => {
    // check if media devices has been prepared,
    // if not, redirect to consent page,
    // and keep the path in appState for later coming back
    if (!appState.initialized) return;
    if (!path.startsWith(AppPaths.tests)) return;
    if (appState.media.videoDeviceId && appState.media.audioDeviceId) return;
    router.replace(AppPaths.prepare);
  }, [path, router]);
}
