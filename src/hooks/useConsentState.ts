import { isConsent } from "@/lib/api/services";
import { appState } from "@/state";
import useSWR, { mutate } from "swr";

export function useConsentState() {
  const { data } = useSWR(appState.authToken?.token, () => isConsent());
  return { isConsent: data };
}

export function revalidateConsentState(value?: boolean) {
  mutate(appState.authToken?.token, value);
}
