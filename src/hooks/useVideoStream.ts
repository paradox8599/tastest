import React from "react";
import { Media, blobToBase64 } from "@/lib/media";
import { useMediaDevices } from "./useMediaDevices";

type Flags = {
  init: boolean;
};

const initialFlags: Flags = {
  init: false,
};

const mimeType = "video/webm";

export function useVideoStream(audio: boolean = false) {
  const { videoDeviceId, audioDeviceId } = useMediaDevices();
  const flags = React.useRef<Flags>(initialFlags);
  const recorder = React.useRef<MediaRecorder>();
  const videoChunks = React.useRef<Blob[]>([]);

  const [videoStream, setVideoStream] = React.useState<MediaStream>();
  const [recording, setRecording] = React.useState<boolean>(false);
  const [recorded, setRecorded] = React.useState<Blob>();
  const [videoUrl, setVideoUrl] = React.useState<string>();

  const getVideoStream = React.useCallback(async () => {
    const stream =
      videoStream ??
      (await Media.getUserMedia({ video: videoDeviceId, audio: null }));
    if (videoStream === void 0) setVideoStream(stream);
    return stream;
  }, [videoStream, setVideoStream, videoDeviceId]);

  const startRecording = React.useCallback(async () => {
    const stream = await getVideoStream();
    if (!stream) return;
    setRecording(true);
    videoChunks.current = [];
    const r = new MediaRecorder(stream, { mimeType });
    recorder.current = r;
    r.ondataavailable = (e) => {
      if (e.data === void 0) return;
      if (e.data.size === 0) return;
      videoChunks.current.push(e.data);
    };
    r.start();
  }, [getVideoStream, setRecording]);

  const stopRecording = React.useCallback(() => {
    const r = recorder.current;
    if (r === void 0) return;
    r.onstop = () => {
      const blob = new Blob(videoChunks.current, { type: mimeType });
      setRecorded(blob);
      setRecording(false);
    };
    r.stop();
  }, [setRecorded]);

  const startCapturing = React.useCallback(() => {
    if (videoStream === void 0) {
      Media.getUserMedia({
        video: videoDeviceId,
        audio: null,
      }).then((stream) => setVideoStream(stream));
    }
  }, [videoDeviceId, videoStream]);

  const stopCapturing = React.useCallback(() => {
    if (videoStream === void 0) return;
    stopRecording();
    videoStream.getTracks().forEach((track) => track.stop()); // stop camera usage
    setVideoStream(void 0);
  }, [videoStream, setVideoStream, stopRecording]);

  // setup recording devices and gather the recording stream
  React.useEffect(() => {
    if (!flags.current.init) {
      flags.current.init = true;
      Media.getUserMedia({
        video: videoDeviceId,
        audio: audio ? audioDeviceId : null,
      }).then((stream) => setVideoStream(stream));
    }
  }, [videoDeviceId, audioDeviceId, audio]);

  // convert blob to playable url when recorded
  React.useEffect(() => {
    if (!recorded) return;
    blobToBase64(recorded).then(setVideoUrl);
  }, [recorded]);

  return {
    videoStream,
    startRecording,
    stopRecording,
    startCapturing,
    stopCapturing,
    recording,
    recorded,
    videoUrl,
    getVideoStream,
  };
}
