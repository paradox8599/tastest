import { appState } from "@/state";
import { useSnapshot } from "valtio";

export function useAuthState() {
  const { authToken } = useSnapshot(appState);
  return { authed: authToken !== void 0 };
}
