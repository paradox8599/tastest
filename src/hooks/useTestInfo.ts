"use client";
import { ProcessResponse } from "@/lib/api/services";
import {
  getCollection,
  getProcess,
  getRoute,
  getTestInfo,
} from "@/lib/api/services";
import { appState } from "@/state";
import React from "react";
import useSWR from "swr";
import { useAuthState } from "./useAuthState";
import { useSnapshot } from "valtio";
import { TestSet } from "@/types";

export function useTestInfo(testId?: string) {
  const { authed } = useAuthState();
  const { info } = useSnapshot(appState);

  const { data: collection } = useSWR(
    authed ? "/collection" : void 0,
    async () => appState.info.collection ?? (await getCollection()),
  );
  const { data: process } = useSWR(
    authed ? "/process" : void 0,
    async () => appState.info.process ?? (await getProcess()),
  );

  const { data: route, mutate: mutateRoute } = useSWR(
    authed ? "/route" : void 0,
    () => getRoute(),
  );

  const { data: testInfo } = useSWR(
    authed ? testId ?? route?.id : void 0,
    (testId) => getTestInfo(testId),
    { revalidateIfStale: false, revalidateOnFocus: false },
  );

  React.useEffect(() => {
    appState.info.collection ??= collection;
    appState.info.process ??= process;
  }, [collection, process]);

  const isStart = React.useMemo(() => {
    const firstTestInSets =
      process?.map((p) => p.tests.find((t) => t.order === 1)!) ?? [];
    if (route === void 0 || firstTestInSets.length === 0) return void 0;
    return firstTestInSets.some((t) => t.test_id === route?.id);
  }, [process, route]);

  const testSet = React.useMemo(() => {
    const p = process?.find((t) => t.id === route?.set);
    const c = collection?.test_sets.find((t) => t.id === route?.set);
    return { process: p, collection: c } as TestSet;
  }, [process, collection, route]);

  return {
    route,
    collection: info.collection,
    process: info.process,
    revalidateRoute: mutateRoute,
    testInfo,
    isStart,
    testSet,
  };
}
