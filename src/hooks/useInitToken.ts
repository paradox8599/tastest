"use client";
import React from "react";
import {
  AppState,
  InitToken,
  InitTokenKeys,
  appState,
  initAppState,
} from "@/state";
import { ReadonlyURLSearchParams, useSearchParams } from "next/navigation";
import { STORAGE_STATE_KEY } from "@/vars";

// validate & extract required fields
function parseParams(params: ReadonlyURLSearchParams) {
  // if params do not contain any of the required fields, return undefined
  if (InitTokenKeys.some((field) => !params.has(field))) return undefined;

  // extract required fields from params
  const searchParams = Object.fromEntries(
    InitTokenKeys.map((f) => [f, params.get(f)]),
  );
  return searchParams as InitToken;
}

/**
 * Get login token from url params, and save to app state.
 */
export function useInitToken() {
  const rawParams = useSearchParams();
  // parse params for login tokens
  React.useEffect(() => {
    // skip if initToken is already set (whether invalid or parsed)
    if (appState.initToken !== void 0) return;
    // parse params from url
    const params = parseParams(rawParams);
    // or get from localstorage if url does not contain required fields
    const storedState = JSON.parse(
      localStorage.getItem(STORAGE_STATE_KEY) ?? JSON.stringify(initAppState),
    ) as AppState;
    const isNewToken =
      params?.token !== void 0
        ? params?.token !== storedState.initToken?.token
        : false;
    Object.assign(appState, {
      ...(isNewToken ? {} : storedState),
      initToken: params ?? storedState.initToken,
      initialized: true,
    });
  }, [rawParams]);
}
