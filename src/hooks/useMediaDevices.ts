import { Media } from "@/lib/media";
import { appState } from "@/state";
import React from "react";
import useSWR from "swr";
import { useSnapshot } from "valtio";

export function useMediaDevices() {
  const { media } = useSnapshot(appState);
  const { data: devices, mutate } = useSWR("media-devices", () =>
    Media.getDevices(),
  );
  const condition = (d: { deviceId: string }) => d.deviceId !== "";
  const hasVideoPermission = devices?.videoDevices.every(condition);
  const hasAudioPermission = devices?.audioDevices.every(condition);

  React.useEffect(() => {
    Media.getUserMedia({}).then((stream) => {
      mutate();
      stream?.getTracks().forEach((track) => track.stop());
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { hasVideoPermission, hasAudioPermission, ...devices, ...media };
}
