import React from "react";
import { usePath } from "./usePath";
import { useRouter } from "next/navigation";
import { useConsentState } from "./useConsentState";
import { AppPathList, AppPaths } from "@/route";
import { appState } from "@/state";

/**
 * Redirect to consent page if not consent yet
 */
export function useConsentCheck() {
  const router = useRouter();
  const path = usePath();
  const { isConsent } = useConsentState();

  React.useEffect(() => {
    if (isConsent === void 0) return;
    if (isConsent === true) return;
    if (AppPathList.indexOf(path) <= AppPathList.indexOf(AppPaths.consent)) {
      return;
    }
    appState.redirect = path;
    router.replace(AppPaths.consent);
  }, [path, isConsent, router]);
}
