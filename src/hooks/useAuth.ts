"use client";
import React from "react";
import useSWR from "swr";
import { useSnapshot } from "valtio";

import { appState } from "@/state";
import { candidateToken } from "@/lib/api/auth";
import { HTTPError } from "@/lib/api/base";
import { useRouter } from "next/navigation";
import { usePath } from "./usePath";
import { AppPaths } from "@/route";

/**
 * Try to login with params from url if not logged in
 * Redirect to home page if still not logged in
 */
export function useAuth() {
  const path = usePath();
  const router = useRouter();
  // initToken can be:
  //  - undefined: not initialized
  //  - null: invalid url params
  const { initToken, initialized, authToken } = useSnapshot(appState);

  useSWR({ initToken, authToken }, async () => {
    try {
      if (!appState.initialized) return;
      if (!appState.initToken) return;
      if (appState.authToken !== void 0) return;
      const newToken = await candidateToken({
        source: appState.initToken!.source,
        token: appState.initToken!.token,
        externalId: appState.initToken!.external_id,
        collectionId: appState.initToken!.collection_id,
      });
      appState.authToken = newToken;
    } catch (e) {
      console.error(e);
      if (e instanceof HTTPError) {
        if (e.status === 401) {
          // prevent from retrying if url token is invalid
          appState.initToken = null;
        }
      }
    }
  });

  React.useEffect(() => {
    if (path === AppPaths.home) return;
    if (initialized && initToken === void 0) {
      router.replace(AppPaths.home);
    }
  }, [initToken, path, initialized, router]);

  return {
    authToken,
    invalid: initToken === null,
    authed: authToken !== void 0,
  };
}
