import { AppPath } from "@/route";
import { usePathname } from "next/navigation";

/**
 * Get path without locale prefix
 */
export function usePath() {
  const path = usePathname();
  return path.replace(/^\/(en|de|zh)/, "/").replace(/\/\//, "/") as AppPath;
}
