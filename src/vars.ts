export const isDev = process.env.NODE_ENV === "development";
export const API_SERVER_URL = process.env.NEXT_PUBLIC_API_SERVER_URL!;
export const STORAGE_STATE_KEY = "appState";
export const ASSETS = {
  videos: {
    positionInstruction:
      process.env.NEXT_PUBLIC_VIDEO_POSITION_INSTRUCTION ??
      "position_instruction.mp4",
  },
  images: {
    utasLogo: process.env.NEXT_PUBLIC_UTAS_LOGO ?? "utas-logo.png",
  },
};
export const CONSENT_AGE = Number.parseInt(
  process.env.NEXT_PUBLIC_CONSENT_AGE ?? "18",
);

export const TEST_ROUTES = {
  ptk: "/tests/audio",
  hand: "/tests/camera",
} as const;

export const TEST_CONFIGS = {
  audioSeconds: Number.parseInt(
    process.env.NEXT_PUBLIC_TEST_CFG_AUDIO_SECONDS ?? "10",
  ),
};
