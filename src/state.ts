import { proxy, subscribe } from "valtio";
import { STORAGE_STATE_KEY } from "./vars";
import { CollectionResponse, ProcessResponse } from "./lib/api/services";

export type AuthToken = { type: string; token: string };

export const InitTokenKeys = [
  "source",
  "token",
  "external_id",
  "collection_id",
] as const;

export type InitToken = {
  [K in typeof InitTokenKeys extends readonly (infer U)[] ? U : never]: string;
};

export type AppState = {
  initialized: boolean;
  initToken?: InitToken | null;
  authToken?: AuthToken;
  redirect?: string;
  media: {
    audioDeviceId?: string;
    videoDeviceId?: string;
  };
  info: {
    collection?: CollectionResponse;
    process?: ProcessResponse;
  };
};

export const initAppState: AppState = {
  initialized: false,
  media: {},
  info: {},
};

export const appState = proxy(initAppState);

function getSerializedState() {
  return JSON.stringify({
    initToken: appState.initToken,
    authToken: appState.authToken,
    media: appState.media,
    info: appState.info,
  });
}

subscribe(appState, () => {
  // only save appState if it has been initialized (load from localstorage)
  if (!appState.initialized) return;
  localStorage.setItem(STORAGE_STATE_KEY, getSerializedState());
});

export function logout() {
  localStorage.removeItem(STORAGE_STATE_KEY);
  window.location.replace("/");
}
