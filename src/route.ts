export type AppPath = (typeof AppPaths)[keyof typeof AppPaths];

export const AppPaths = {
  home: "/",
  positionInstruction: "/position-instruction",
  consent: "/consent",
  prepare: "/prepare",
  tests: "/tests",
  questionnaire: "/questionnaire",
  finish: "/finish",
} as const;

export const AppPathList = Object.values(AppPaths);
