import { CollectionResponse, ProcessResponse } from "./lib/api/services";

export enum TestSteps {
  instruction = 0,
  countDown = 1,
  recording = 2,
  finish = 3,
}

export type TestSet = {
  process?: ProcessResponse[number];
  collection?: CollectionResponse["test_sets"][number];
};
