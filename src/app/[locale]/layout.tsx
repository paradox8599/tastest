import "./globals.css";
import { Inter } from "next/font/google";
import type { Metadata } from "next";
import { getMessages } from "next-intl/server";

import { AuthProvider } from "@/providers/auth-provider";
import { NextIntlClientProvider } from "next-intl";
import { AppMessageProvider } from "@/providers/app-message-provider";
import { DataProvider } from "@/providers/data-provider";

import AppBar from "@/components/app-bar";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "TAS Test",
  description: "",
};

export default async function RootLayout({
  children,
  params: { locale },
}: Readonly<{ children: React.ReactNode; params: { locale: string } }>) {
  const messages = await getMessages();
  return (
    <html lang={locale}>
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
      </head>
      <body className={inter.className}>
        {/* next-intl */}
        <NextIntlClientProvider messages={messages}>
          <AuthProvider>
            <DataProvider>
              <AppMessageProvider>
                <AppBar className="sticky top-0 z-50" />
                {children}
              </AppMessageProvider>
            </DataProvider>
          </AuthProvider>
        </NextIntlClientProvider>
      </body>
    </html>
  );
}
