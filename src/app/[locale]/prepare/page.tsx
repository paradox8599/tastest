"use client";
import { MdFilledButton, MdOutlinedButton } from "@/components/md";
import { DeviceSetDialog } from "@/components/prepare/device-set-dialog";
import { InstructionProgressBar } from "@/components/prepare/instructions-progress-bar";
import { InstructionAudio } from "@/components/prepare/step-audio";
import { InstructionCamera } from "@/components/prepare/step-camera";
import { useMediaDevices } from "@/hooks/useMediaDevices";
import { AppPaths } from "@/route";
import { appState } from "@/state";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";
import React from "react";
import { toast } from "react-toastify";

type DialogStates = {
  2: boolean;
  3: boolean;
};
const initDialogStates = {
  2: false,
  3: false,
};

const STEP_COUNT = 3;

export default function PreparePage() {
  const router = useRouter();
  const tr = useTranslations("preparePage");
  const { hasVideoPermission, hasAudioPermission } = useMediaDevices();
  const [step, setStep] = React.useState(1);
  const [wellDoneDialogs, setWellDoneDialogs] =
    React.useState<DialogStates>(initDialogStates);
  const prevStep = () => {
    toast.dismiss();
    setStep(Math.max(step - 1, 1));
  };
  const nextStep = () => {
    toast.dismiss();
    setStep(Math.min(step + 1, STEP_COUNT));
  };
  const progressCheck = () => {
    let stopHere = false;
    switch (step) {
      case 1:
        nextStep();
        break;
      case 2:
        // check if camera is set
        stopHere = appState.media.videoDeviceId === void 0;
        if (stopHere) {
          return toast.warn(
            tr("dialog.device.content", { device: tr("cameraLowercase") }),
          );
        }
        setWellDoneDialogs({ 2: true, 3: false });
        break;
      case 3:
        // check if microphone is set
        stopHere = appState.media.audioDeviceId === void 0;
        if (stopHere) {
          return toast.warn(
            tr("dialog.device.content", { device: tr("microphoneLowercase") }),
          );
        }
        setWellDoneDialogs({ 2: false, 3: true });
        break;
    }
  };
  return (
    <main className="w-full text-center">
      <h1 className="text-2xl p-2">{tr("title")}</h1>
      <div className="px-2">
        <div>
          <div className="bg-surface px-1 py-2 rounded-xl text-center">
            <h3 className="text-xl font-semibold py-2">{tr("instructions")}</h3>
            <InstructionProgressBar
              value={step}
              steps={STEP_COUNT}
              className="mx-4 my-2 z-0"
            />
            {/* Instruction Body */}
            <div className="flex flex-row justify-start items-start px-2 py-2">
              {step === 1 && tr("inst.i1")}
              {step === 2 && hasVideoPermission && (
                <InstructionCamera tr={tr} />
              )}
              {step === 3 && hasAudioPermission && <InstructionAudio tr={tr} />}

              {/* Error Messages */}
              {((step === 2 && !hasVideoPermission) ||
                (step === 3 && !hasAudioPermission)) && (
                <p className="flex-center bg-surface rounded p-2 m-2">
                  {tr("errorMessages.permission", {
                    device: !hasVideoPermission
                      ? tr("cameraLowercase")
                      : tr("microphoneLowercase"),
                  })}
                </p>
              )}
            </div>
            {/* Step Navigation Buttons */}
            <div className="flex flex-row justify-evenly py-2">
              {/* Prev Button */}
              {step > 1 && (
                <MdOutlinedButton onClick={prevStep}>
                  <div className="flex items-center">{tr("buttons.prev")}</div>
                </MdOutlinedButton>
              )}
              {/* Next Button */}
              <MdFilledButton onClick={progressCheck}>
                <div className="flex items-center">{tr("buttons.next")}</div>
              </MdFilledButton>
            </div>
          </div>

          <DeviceSetDialog
            open={step === 2 && wellDoneDialogs[2]}
            device={tr("cameraLowercase")}
            onClose={() => {
              setWellDoneDialogs({ 2: false, 3: false });
              nextStep();
            }}
          />
          <DeviceSetDialog
            open={step === 3 && wellDoneDialogs[3]}
            device={tr("microphoneLowercase")}
            onClose={() => {
              setWellDoneDialogs({ 2: false, 3: false });
              router.push(AppPaths.tests);
            }}
          />
        </div>
      </div>
    </main>
  );
}
