"use client";

import { useTranslations } from "next-intl";

import { API_PATHS } from "@/lib/api/routes";
import React from "react";
import { MdFilledButton, MdOutlinedButton } from "@/components/md";
import Link from "next/link";
import { ASSETS } from "@/vars";
import { tw } from "@/style";
import { AppPaths } from "@/route";

export default function PositionInstructionPage() {
  const vRef = React.useRef<HTMLVideoElement>(null);
  const [videoFinish, setVideoFinish] = React.useState(false);
  const tr = useTranslations("positionInstructionPage");

  const nextPagePath = AppPaths.prepare;

  const play = React.useCallback(
    (restart: boolean = false) => {
      const v = vRef.current;
      if (!v) return;
      if (restart) v.currentTime = 0;
      v.play();
      setVideoFinish(false);
    },
    [setVideoFinish],
  );

  React.useEffect(() => {
    play();
  }, [play]);

  return (
    <main className="flex-center">
      <section>
        <video
          playsInline
          controls
          ref={vRef}
          className={tw("w-full", "transition-all", videoFinish ? "blur" : "")}
          onEnded={() => setVideoFinish(true)}
        >
          <source
            src={
              API_PATHS.services.media(ASSETS.videos.positionInstruction).href
            }
            type="video/mp4"
          />
        </video>
        <div className="w-full flex flex-col items-center gap-2 mt-2 p-4">
          {!videoFinish && (
            <Link href={nextPagePath} className="w-full">
              <MdOutlinedButton className="w-full">
                {tr("buttons.skip")}
              </MdOutlinedButton>
            </Link>
          )}

          {videoFinish && (
            <MdOutlinedButton onClick={() => play(true)} className="w-full">
              {tr("buttons.replay")}
            </MdOutlinedButton>
          )}

          {videoFinish && (
            <Link href={nextPagePath} className="w-full">
              <MdFilledButton className="w-full">
                {tr("buttons.continue")}
              </MdFilledButton>
            </Link>
          )}
        </div>
      </section>
    </main>
  );
}
