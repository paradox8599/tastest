"use client";

import React from "react";
import {
  MdFilledButton,
  MdOutlinedTextField,
  MdOutlinedButton,
  MdRadio,
} from "@/components/md";
import { useTranslations } from "next-intl";
import { Formik } from "formik";
import { CONSENT_AGE, isDev } from "@/vars";
import { useRouter } from "next/navigation";
import CalendarInput from "@/components/consent/calendar-input";
import { consent, type ConsentBody } from "@/lib/api/actions";
import { toast } from "react-toastify";
import { useConsentState } from "@/hooks/useConsentState";
import { ViewConsentFormButton } from "@/components/consent/view-consent-form-button";
import { appState } from "@/state";
import { AppPaths } from "@/route";

export default function ConsentPage() {
  const tr = useTranslations("consentPage");
  const router = useRouter();
  const { isConsent } = useConsentState();

  const gotoNextPage = React.useCallback(() => {
    toast.dismiss();
    console.log(appState.redirect);
    router.push(appState.redirect ?? AppPaths.prepare);
    appState.redirect = void 0;
  }, [router]);

  React.useEffect(() => {
    // [isConsent] will be set to true when below consent() completes
    if (isConsent) gotoNextPage();
  }, [isConsent, gotoNextPage]);

  return (
    <main className="px-4 smd:px-8">
      <ViewConsentFormButton text={tr("buttons.viewConsentForm")} />
      <div className="my-2">{tr("instruction")}</div>
      <Formik
        initialValues={
          {
            firstName: "",
            lastName: "",
            gender: undefined,
            dob: undefined,
            consent: "",
          } as {
            firstName: ConsentBody["first_name"];
            lastName: ConsentBody["last_name"];
            gender?: ConsentBody["gender"];
            dob?: ConsentBody["dob"];
            consent: string;
          }
        }
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            if (values.consent !== "agree") throw tr("form.consentWarn");

            const age =
              new Date(Date.now() - values.dob!.getTime()).getUTCFullYear() -
              1970;
            if (age < CONSENT_AGE) throw tr("form.dobWarn");

            // [isConsent] will be set to true when consent action is completed
            // Above useEffect which listens to [isConsent] will be triggered
            const r = await consent({
              first_name: values.firstName,
              last_name: values.lastName,
              dob: values.dob! as ConsentBody["dob"],
              gender: values.gender! as ConsentBody["gender"],
            });

            if (r !== "succeed") {
              toast.error(tr("form.consentSubmitError"));
            }
          } catch (e) {
            if (typeof e === "string") {
              toast.warn(e);
            } else {
              toast.error("Unexpected error occurred");
            }
          } finally {
            setSubmitting(false);
          }
        }}
      >
        {({
          values,
          handleSubmit,
          handleChange,
          handleBlur,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit} className="w-full flex flex-col gap-4">
            {/* First Name */}
            <MdOutlinedTextField
              required
              name="firstName"
              label={tr("form.firstName")}
              onBlur={handleBlur}
              value={values.firstName}
              onInput={handleChange}
              autocomplete="given-name"
            />
            {/* Last Name */}
            <MdOutlinedTextField
              required
              name="lastName"
              label={tr("form.lastName")}
              onBlur={handleBlur}
              value={values.lastName}
              onInput={handleChange}
              autocomplete="family-name"
            />
            {/*  DOB */}
            <CalendarInput
              name="dob"
              label={tr("form.dob")}
              value={values.dob}
              onDayPick={(d) => setFieldValue("dob", d)}
            />

            {/* gender radio */}
            <div role="radiogroup">
              <div className="mb-1">{tr("form.gender")}*</div>
              {[
                { value: "Male", label: tr("form.male") },
                { value: "Female", label: tr("form.female") },
                { value: "Other", label: tr("form.other") },
                { value: "Unknown", label: tr("form.unknown") },
              ].map((g, i) => (
                <label key={i.toString()} className="inline-flex gap-1 px-1">
                  <MdRadio
                    required
                    name="gender"
                    value={g.value}
                    aria-label={g.label}
                    onInput={handleChange}
                  />
                  {g.label}
                </label>
              ))}
            </div>

            {/* consent radio} */}
            <div role="radiogroup" className="flex flex-col">
              <label>
                <MdRadio
                  required
                  className="mr-1"
                  name="consent"
                  value="agree"
                  aria-label="agree"
                  onInput={handleChange}
                />
                {tr("form.agree")}
              </label>
              <label>
                <MdRadio
                  className="mr-1"
                  name="consent"
                  value="disagree"
                  aria-label="disagree"
                  onInput={handleChange}
                />
                {tr("form.disagree")}
              </label>
            </div>

            <div>
              {isDev && (
                <MdOutlinedButton
                  type="reset"
                  className="w-full my-2"
                  onClick={async () => {
                    await consent({
                      first_name: "dev",
                      last_name: "dev",
                      dob: new Date("1970-01-01"),
                      gender: "Other",
                    });
                  }}
                >
                  {tr("buttons.skip")}
                </MdOutlinedButton>
              )}
              <MdFilledButton className="w-full" type="submit">
                {tr("buttons.submit")}
              </MdFilledButton>
            </div>
          </form>
        )}
      </Formik>
    </main>
  );
}
