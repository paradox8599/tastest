"use client";
import Card from "@/components/questionnaire/card";
import { MdFilledButton, MdLinearProgress } from "@/components/md";
import { useTranslations } from "next-intl";
import {
  getQuestionnaire,
  QuestionOption,
  QuestionnaireResponse,
  saveQuestionnaire,
} from "@/lib/api/questionnaire";
import { useEffect, useState } from "react";
import { getCollection } from "@/lib/api/services";
import Question from "@/components/questionnaire/question";
import { Formik } from "formik";
import { tw } from "@/style";
import Complete from "@/components/questionnaire/complete";
import { toast } from "react-toastify";

export default function Questionnaire() {
  const tr = useTranslations("questionnairePage.buttons");
  const trStart = useTranslations("questionnairePage.start");
  const trEnd = useTranslations("questionnairePage.end");
  const trForm = useTranslations("questionnairePage.form");

  const [currentStep, setCurrentStep] = useState(0);
  const [questions, setQuestions] = useState<Array<QuestionOption>>([]);
  const [questionnaire, setQuestionnaire] =
    useState<QuestionnaireResponse>(null);

  const fetchQuestionGroups = async () => {
    const res = await getCollection();
    const r = await getQuestionnaire(res.feedback_id);
    setQuestionnaire(r);
    if (r && r.question_groups_with_questions) {
      setQuestions(r.question_groups_with_questions[0].questions || []);
    }
  };

  const submit = async (values: any, { setSubmitting }: any) => {
    if (Object.keys(values).length === 0) return;

    setSubmitting(true);
    try {
      const r = await saveQuestionnaire(values);
      console.log(r);
      if (r !== "succeed") {
        toast.error(trForm("submitError"));
      }
    } catch (error) {
      if (typeof error === "string") {
        toast.warn(error);
      } else {
        toast.error(trForm("unexpected"));
      }
    } finally {
      setSubmitting(false);
    }
  };

  useEffect(() => {
    fetchQuestionGroups();
  }, []);

  const Start = (
    <div className="flex flex-col gap-4">
      <p className="text-sm text-black font-normal">
        {trStart.rich("i1", {
          tap: (tap) => (
            <span className="font-semibold text-[#002060]">{tap}</span>
          ),
          talk: (talk) => (
            <span className="font-semibold text-[#0070C0]">{talk}</span>
          ),
        })}
      </p>
      <p className="text-sm text-black font-normal">{trStart("i2")}</p>
    </div>
  );

  const End = (
    <p className="text-base text-black font-semibold text-center whitespace-pre-wrap">
      {trEnd("content")}
    </p>
  );

  return (
    <main className="w-full pt-6 pr-3 pl-4 box-border">
      <Formik
        initialValues={questions.reduce(
          (a, b) => ({ ...a, [b.order]: null }),
          {},
        )}
        onSubmit={submit}
      >
        {({ values, handleSubmit, handleChange, handleBlur }) => (
          <>
            {currentStep > questions.length + 1 ? (
              <Complete />
            ) : (
              <form onSubmit={handleSubmit}>
                <div className="flex flex-col gap-4">
                  {currentStep === 1 ? (
                    <p className="text-base font-semibold text-black">
                      {questionnaire?.title}
                    </p>
                  ) : null}
                  {currentStep === 1 ? (
                    <p className="text-sm font-normal text-black">
                      {questionnaire?.des}
                    </p>
                  ) : null}
                  {currentStep !== 0 ? (
                    <MdLinearProgress
                      max={questions.length + 1}
                      value={currentStep}
                      className={tw(
                        "[--md-linear-progress-track-height:12px]",
                        "[--md-linear-progress-track-shape:12px]",
                        "[--md-linear-progress-active-indicator-height:12px]",
                        "[--md-sys-color-primary:#5DB075]",
                        "[--md-sys-color-surface-container-highest:#D9D9D9]",
                      )}
                    ></MdLinearProgress>
                  ) : null}
                  <Card>
                    {currentStep === 0 ? Start : null}
                    {currentStep === questions.length + 1 ? End : null}
                    {currentStep > 0 && currentStep < questions.length + 1 ? (
                      <Question
                        {...questions[currentStep - 1]}
                        values={values}
                        change={handleChange}
                        blur={handleBlur}
                      />
                    ) : null}

                    <div className="flex gap-12 justify-center mt-6">
                      {currentStep === 0 ? (
                        <>
                          <MdFilledButton
                            onClick={(event) => {
                              setCurrentStep(questions.length + 2);
                              event.stopPropagation();
                            }}
                          >
                            {tr("skip")}
                          </MdFilledButton>
                          <MdFilledButton
                            onClick={(event) => {
                              setCurrentStep(currentStep + 1);
                              event.stopPropagation();
                            }}
                          >
                            {tr("yes")}
                          </MdFilledButton>
                        </>
                      ) : null}
                      {currentStep > 0 &&
                      currentStep <= questions.length + 1 ? (
                        <MdFilledButton
                          type={
                            currentStep === questions.length
                              ? "submit"
                              : "button"
                          }
                          onClick={(event) => {
                            setCurrentStep(currentStep + 1);
                            event.stopPropagation();
                          }}
                          className={tw(
                            "[--md-filled-button-leading-space:60px]",
                            "[--md-filled-button-trailing-space:60px]",
                          )}
                        >
                          {tr("next")}
                        </MdFilledButton>
                      ) : null}
                    </div>
                  </Card>
                </div>
              </form>
            )}
          </>
        )}
      </Formik>
    </main>
  );
}
