"use client";

import React from "react";
import { useRouter, useSearchParams } from "next/navigation";

import { TestSteps } from "@/types";
import { Loading } from "@/components/loading";
import { MdFilledButton, MdFilledTonalButton } from "@/components/md";
import { CameraPreview } from "@/components/camera-preview";

import { useTestInfo } from "@/hooks/useTestInfo";
import { useVideoStream } from "@/hooks/useVideoStream";

import { CameraTestParams, getRoute } from "@/lib/api/services";
import { API_PATHS } from "@/lib/api/routes";
import { TestTimer } from "@/components/tests/test-timer";
import { useTranslations } from "next-intl";
import TestProgress from "@/components/tests/test-progress";
import CountDown from "@/components/count-down/count-down";
import TestPlayback from "@/components/tests/test-playback";
import { skipTest, submitTest } from "@/lib/api/actions";
import { AppPaths } from "@/route";
import { toast } from "react-toastify";

export default function TestCameraPage() {
  const tr = useTranslations("testsPage");
  const router = useRouter();
  const params = useSearchParams();
  const testId = React.useMemo(() => params.get("id") ?? void 0, [params]);

  const { testInfo, testSet, route, revalidateRoute } = useTestInfo(testId);
  const testParams = testInfo?.params as CameraTestParams | undefined;

  const [step, setStep] = React.useState<TestSteps>(0);
  const recordingStarted = React.useRef<boolean>(false);

  const {
    videoStream,
    recording,
    startRecording,
    stopRecording,
    videoUrl,
    startCapturing,
    recorded,
  } = useVideoStream();

  const start = React.useCallback(async () => {
    if (recording || recordingStarted.current) return;
    startCapturing();
    recordingStarted.current = true;
    startRecording();
    const recordMs = (testParams!.autoRecordTime + 1) * 1000;
    await new Promise((r) => setTimeout(r, recordMs));
    stopRecording();
    recordingStarted.current = false;
  }, [testParams, startRecording, stopRecording, recording, startCapturing]);

  // validate test info, otherwise redirect to main tests page
  React.useEffect(() => {
    if (testInfo === void 0) return;
    const paramId = params.get("id");
    if (paramId === null || paramId !== testInfo?.id)
      router.replace(AppPaths.tests);
  }, [params, router, testInfo]);

  React.useEffect(() => {
    switch (step) {
      case TestSteps.instruction:
        break;
      case TestSteps.countDown:
        break;
      case TestSteps.recording:
        if (!testInfo || recordingStarted.current) return;
        if (testParams?.autoStart) start().then(() => setStep(step + 1));
        break;
      case TestSteps.finish:
        // NOTE: do not stop capturing in case user click on Try Again
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step]);

  if (!testInfo) {
    return (
      <main>
        <Loading />
      </main>
    );
  }

  return (
    <main>
      {/* 1. Instruction */}
      {step === TestSteps.instruction && (
        <div>
          <div className="flex justify-end p-2">
            <MdFilledTonalButton
              onClick={async () => {
                if (!route) return;
                await skipTest({ test_set_id: route.set, test_id: route.id });
                const r = await getRoute();
                revalidateRoute(r);
                router.push(AppPaths.tests);
              }}
            >
              {tr("buttons.skip")}
            </MdFilledTonalButton>
          </div>
          <video
            autoPlay
            playsInline
            controls
            src={API_PATHS.services.media(testParams!.instructionVideo).href}
            className="w-full"
          />
          <h1 className="py-4 flex-center text-2xl font-semibold">
            {testInfo!.name}
          </h1>
          <TestProgress
            total={testSet.process?.tests.length!}
            current={
              testSet.process?.tests
                .sort((a, b) => a.order - b.order)
                .findIndex((t) => t.test_id === testInfo.id)! + 1
            }
          />
          <div className="px-4">
            <MdFilledButton
              onClick={() => setStep(step + 1)}
              className="w-full"
            >
              {tr("buttons.next")}
            </MdFilledButton>
          </div>
        </div>
      )}

      {/* 2. Count down */}
      {step === TestSteps.countDown &&
        (() => {
          const duration = testParams?.autoStartPrepareTime ?? 5;
          return (
            <CountDown
              title={testInfo.name}
              duration={duration}
              text={tr("getReady.cameraText", { seconds: duration })}
              onComplete={() => setStep(step + 1)}
            />
          );
        })()}

      {/* 3. Recording */}
      {step === TestSteps.recording && (
        <div className="flex flex-col gap-2">
          <CameraPreview videoStream={videoStream} recording={recording} />
          <div className="px-2">
            <TestTimer durationMs={testParams!.autoRecordTime * 1000} />
          </div>
        </div>
      )}

      {/* 4. Finish & Playback */}
      {step === TestSteps.finish && (
        <TestPlayback
          onRetry={() => setStep((step) => step - 1)}
          onNext={async () => {
            const t = () => toast.warn(tr("toast.uploadError"));
            if (!recorded) return t();
            const r = await submitTest({
              test_id: testInfo.id,
              test_set_id: testSet.process!.id,
              file: recorded,
            });
            if (r === "succeed") {
              return router.push(AppPaths.tests);
            } else {
              t();
            }
          }}
        >
          {videoUrl && (
            <video
              controls
              playsInline
              src={videoUrl}
              className="w-2/3 rounded-lg"
            />
          )}
        </TestPlayback>
      )}
    </main>
  );
}
