"use client";
import TestInfo from "@/components/tests/test-info";
import { Loading } from "@/components/loading";
import { usePath } from "@/hooks/usePath";
import { useTestInfo } from "@/hooks/useTestInfo";
import { AppPaths } from "@/route";
import { useRouter } from "next/navigation";
import React from "react";

export default function TestsPage() {
  const router = useRouter();
  const path = usePath();
  const { route, revalidateRoute, isStart, testSet } = useTestInfo();

  // re-fetch the test route when path changes
  React.useEffect(() => {
    if (path !== AppPaths.tests) return;
    revalidateRoute();
  }, [path, revalidateRoute]);

  const gotoNextStep = React.useCallback(() => {
    // [route] is path for the original website, translating for this app
    if (route?.valid) {
      return router.replace(route.fullPath);
    }
    if (route?.raw.startsWith("/feedback")) {
      return router.replace(AppPaths.questionnaire);
    }
    if (route?.raw.startsWith("/finished")) {
      return router.replace(AppPaths.home);
    }
  }, [route, router]);

  const isTestRoute = React.useMemo(
    () => route?.raw.startsWith("/test"),
    [route],
  );

  // redirect to current test page with params (from route api)
  React.useEffect(() => {
    if (isStart) return;
    gotoNextStep();
  }, [isStart, gotoNextStep]);

  return (
    <main>
      {isTestRoute && !route?.valid && <div>Invalid test arguments</div>}

      {route?.valid && !isStart && <Loading />}

      {route?.valid && isStart && (
        <TestInfo
          title={testSet.process?.name ?? ""}
          total={testSet.process?.tests.length ?? 0}
          instructions={
            testSet.collection?.instructions
              .sort((a, b) => a.index - b.index)
              .map((i) => i.content) ?? []
          }
          onNext={gotoNextStep}
        />
      )}
    </main>
  );
}
