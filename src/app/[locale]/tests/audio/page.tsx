"use client";

import React from "react";
import { useRouter, useSearchParams } from "next/navigation";

import { TestSteps } from "@/types";
import { Loading } from "@/components/loading";
import { MdFilledButton, MdFilledTonalButton } from "@/components/md";

import { useTestInfo } from "@/hooks/useTestInfo";

import { AudioTestParams, getRoute } from "@/lib/api/services";
import { API_PATHS } from "@/lib/api/routes";
import { TestTimer } from "@/components/tests/test-timer";
import { useTranslations } from "next-intl";
import { VoiceVisualizer, useVoiceVisualizer } from "react-voice-visualizer";
import { TEST_CONFIGS } from "@/vars";
import { RecordingIndicator } from "@/components/tests/recording-frame";
import CountDown from "@/components/count-down/count-down";
import TestPlayback from "@/components/tests/test-playback";
import { skipTest, submitTest } from "@/lib/api/actions";
import { AppPaths } from "@/route";
import TestProgress from "@/components/tests/test-progress";
import { blobToBase64 } from "@/lib/media";
import { toast } from "react-toastify";

export default function TestAudioPage() {
  const tr = useTranslations("testsPage");
  const router = useRouter();
  const params = useSearchParams();
  const testId = React.useMemo(() => params.get("id") ?? void 0, [params]);

  const { testInfo, testSet, route, revalidateRoute } = useTestInfo(testId);
  const testParams = testInfo?.params as AudioTestParams | undefined;

  const [step, setStep] = React.useState<TestSteps>(0);
  const recordingStarted = React.useRef<boolean>(false);

  const controls = useVoiceVisualizer();
  const { recordedBlob } = controls;
  const [audioUrl, setAudioUrl] = React.useState<string>();

  const startRecording = React.useCallback(async () => {
    if (!testInfo || recordingStarted.current) return;
    recordingStarted.current = true;
    controls.startRecording();
    const recordMs = (TEST_CONFIGS.audioSeconds + 0.5) * 1000;
    await new Promise((r) => setTimeout(r, recordMs));
    controls.stopRecording();
    recordingStarted.current = false;
  }, [controls, testInfo]);

  React.useEffect(() => {
    if (recordedBlob === null) return;
    blobToBase64(recordedBlob).then((v) => setAudioUrl(v));
  }, [recordedBlob]);

  // validate test info, otherwise redirect to main tests page
  React.useEffect(() => {
    if (testInfo === void 0) return;
    const paramId = params.get("id");
    if (paramId === null || paramId !== testInfo?.id)
      router.replace(AppPaths.tests);
  }, [params, router, testInfo]);

  React.useEffect(() => {
    switch (step) {
      case TestSteps.instruction:
        break;
      case TestSteps.countDown:
        setStep(step + 1);
        break;
      case TestSteps.recording:
        startRecording().then(() => setStep(step + 1));
        break;
      case TestSteps.finish:
        // NOTE: recording won't stop after timer ends, but it works here
        controls.stopRecording();
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step]);

  if (!testInfo) {
    return (
      <main>
        <Loading />
      </main>
    );
  }

  return (
    <main>
      {/* 1. Instruction */}
      {step === TestSteps.instruction && (
        <div>
          <div className="flex justify-end p-2">
            <MdFilledTonalButton
              onClick={async () => {
                if (!route) return;
                await skipTest({ test_set_id: route.set, test_id: route.id });
                const r = await getRoute();
                revalidateRoute(r);
                router.push(AppPaths.tests);
              }}
            >
              {tr("buttons.skip")}
            </MdFilledTonalButton>
          </div>
          <video
            autoPlay
            playsInline
            controls
            src={API_PATHS.services.media(testParams!.videoInstruction).href}
            className="w-full"
          />
          <h1 className="py-4 flex-center text-2xl font-semibold">
            {testInfo!.name}
          </h1>

          <TestProgress
            total={testSet.process?.tests.length!}
            current={
              testSet.process?.tests
                .sort((a, b) => a.order - b.order)
                .findIndex((t) => t.test_id === testInfo.id)! + 1
            }
          />

          <div className="px-4">
            <MdFilledButton
              onClick={() => setStep(step + 1)}
              className="w-full"
            >
              {tr("buttons.next")}
            </MdFilledButton>
          </div>
        </div>
      )}

      {/* 2. Count down */}
      {step === TestSteps.countDown && (
        <CountDown
          title={testInfo.name}
          duration={5}
          text={tr("getReady.audioText", { seconds: 5 })}
          onComplete={() => setStep(step + 1)}
        />
      )}

      {/* 3. Recording */}
      {step === TestSteps.recording && (
        <div className="flex flex-col gap-2">
          <RecordingIndicator className="py-12">
            <VoiceVisualizer
              controls={controls}
              isControlPanelShown={false}
              barWidth={4}
              height={150}
              rounded={5}
              mainBarColor="#888"
              secondaryBarColor="#888"
              defaultAudioWaveIconColor="#888"
              defaultMicrophoneIconColor="#888"
            />
          </RecordingIndicator>
          <TestTimer durationMs={TEST_CONFIGS.audioSeconds * 1000} />
        </div>
      )}

      {/* 4. Finish & Playback */}
      {step === TestSteps.finish && (
        <TestPlayback
          onRetry={() => setStep((step) => step - 1)}
          onNext={async () => {
            const t = () => toast.warn(tr("toast.uploadError"));
            if (!recordedBlob) return t();
            const r = await submitTest({
              test_id: testInfo.id,
              test_set_id: testSet.process!.id,
              file: recordedBlob,
            });
            if (r === "succeed") {
              return router.push(AppPaths.tests);
            } else {
              t();
            }
          }}
        >
          <audio src={audioUrl} controls />
        </TestPlayback>
      )}
    </main>
  );
}
