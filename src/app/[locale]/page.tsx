"use client";

import { useTranslations } from "next-intl";

import { useAuthState } from "@/hooks/useAuthState";
import { MdFilledButton } from "@/components/md";
import { LogoutButton } from "@/components/buttons/logout-button";
import { isDev } from "@/vars";
import Link from "next/link";
import TestCard from "@/components/test-card";
import { AppPaths } from "@/route";

export default function Home() {
  const { authed } = useAuthState();
  const tr = useTranslations("indexPage");
  return (
    <main className="flex-center flex-col">
      <section>
        {/* TODO: not authed message, for dev only, remove on production, display a more user friendly message */}
        {isDev && !authed && <>Not Authed</>}
        {authed && (
          <div className="flex flex-col justify-center items-center gap-4">
            <div>
              <Link href={AppPaths.positionInstruction}>
                <MdFilledButton>{tr("buttons.start")}</MdFilledButton>
              </Link>
            </div>
            <div>
              <LogoutButton />
            </div>
            {isDev && <TestCard />}
          </div>
        )}
      </section>
    </main>
  );
}
