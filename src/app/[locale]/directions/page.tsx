"use client";

import { MdFilledButton } from "@/components/md";
import { useTranslations } from "next-intl";
import { tw } from "@/style";
import Directions from "@/components/directions";

export default function DirectionsPage() {
  const tr = useTranslations("directionsPage");

  return (
    <main className="w-full pt-24 pr-3 pl-4 box-border flex flex-col gap-9">
      <Directions total={7}></Directions>
      <div className="text-center">
        <MdFilledButton
          className={tw(
            "[--md-filled-button-leading-space:60px]",
            "[--md-filled-button-trailing-space:60px]",
          )}
        >
          {tr("buttons.next")}
        </MdFilledButton>
      </div>
    </main>
  );
}
