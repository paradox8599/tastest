import { notFound } from "next/navigation";
import { getRequestConfig } from "next-intl/server";

// Can be imported from a shared config
export const locales = ["en", "de", "zh"];

export const localeNames = {
  en: "English",
  de: "Deutsch",
  zh: "中文",
} as const;

export default getRequestConfig(async ({ locale }) => {
  // Validate that the incoming `locale` parameter is valid
  if (!locales.includes(locale as any)) notFound();

  return {
    messages: (await import(`../messages/output/${locale}.json`)).default,
  };
});
