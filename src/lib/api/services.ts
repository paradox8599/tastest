import { TEST_ROUTES } from "@/vars";
import { sendRequest } from "./base";
import { API_PATHS } from "./routes";

export async function isConsent() {
  return await sendRequest<boolean>(API_PATHS.services.isConsent());
}

export async function isFeedbackEnabled() {
  return await sendRequest<boolean>(API_PATHS.services.isFeedbackEnabled());
}

export async function hasFeedback() {
  return await sendRequest<boolean>(API_PATHS.services.hasFeedback());
}

export async function whoAmI() {
  type WhoAmIResponse = {
    external_id: string;
    source: string;
    is_dev: boolean;
    is_consent: boolean;
    id: string;
    info: {
      candidate_id: string;
      first_name: string;
      middle_name: string | null;
      last_name: string;
      dob: string;
      gender: string;
      mobile: string | null;
      email: string | null;
      extended_info: unknown | null;
    };
    log: {
      candidate_id: string;
      last_login: string;
      date_joined: string;
      extended_info: unknown | null;
    };
  };
  return await sendRequest<WhoAmIResponse>(API_PATHS.services.whoAmI());
}

export type CollectionResponse = {
  attempt_num: number;
  consent_form: null | string;
  des: null | string;
  end_time: string;
  feedback_id: string;
  has_feedback: boolean;
  id: string;
  introduction_video: null | string;
  is_active: boolean;
  name: string;
  platform: string;
  start_time: string;
  test_sets: {
    collection_id: string;
    des: null | string;
    icon: null | string;
    id: number;
    instructions: {
      content: string;
      id: string;
      index: number;
      icon: {
        name: string;
        color: string;
      };
    }[];
    name: string;
    order: number;
  }[];
};
export async function getCollection() {
  return await sendRequest<CollectionResponse>(API_PATHS.services.collection());
}

export type ProcessResponse = {
  collection_id: string;
  des: null | string;
  icon: null | string;
  id: number;
  name: null | string;
  order: number;
  tests: {
    order: number;
    test_id: string;
    collection_test_set_id: number;
  }[];
}[];
export async function getProcess() {
  return await sendRequest<ProcessResponse>(API_PATHS.services.process());
}

export type ParsedTestRoute = {
  raw: string;
  valid: boolean;
  id: string;
  path?: "/tests/audio" | "/tests/camera";
  inst: boolean;
  set: number;
  fullPath: string;
};
export function parseRoute(route: string): ParsedTestRoute {
  if (!route.includes("?")) route += "?";
  const [pathStr, pStr] = route.split("?");
  const params: { set: string; instructions: "0" | "1" } = Object.fromEntries(
    pStr.split("&").map((p) => p.split("=")),
  );
  const [_test, _movement, type, id] = pathStr.replace(/^\//, "").split("/");
  const path = ["hand", "ptk"].includes(type)
    ? TEST_ROUTES[type as "hand" | "ptk"]
    : undefined;
  const inst = params.instructions === "1";
  return {
    raw: route,
    valid: path !== void 0,
    id,
    path,
    inst,
    set: Number.parseInt(params.set),
    fullPath: `${path}?id=${id}&inst=${inst}`,
  };
}

export async function getRoute() {
  return await sendRequest<string>(API_PATHS.services.route())
    // --
    .then(parseRoute);
}

export async function getTestRoute({
  testId,
  set,
}: {
  testId: string;
  set: number;
}) {
  return await sendRequest<string>(
    API_PATHS.services.testRoute({ testId, set }),
  ).then(parseRoute);
}

export type CameraTestParams = {
  audioInstruction: string; // path
  autoRecord: boolean;
  autoRecordTime: number;
  autoStart: boolean;
  autoStartPrepareTime: number;
  autoStop: boolean;
  beepAudio: string; // path
  disableUsrOperation: boolean;
  enableBeep: boolean;
  inProcessDetection: boolean;
  instructionVideo: string; // path
  recordAudio: boolean;
  recordLeftHand: boolean;
  recordRightHand: boolean;
  showFlashLight: boolean;
  showNumSeries: boolean;
};

export type AudioTestParams = {
  recReminder: string;
  reminderWinArry: string;
  videoInstruction: string; // path
};

function isCameraTestParams(
  params: CameraTestParams | AudioTestParams,
): params is CameraTestParams {
  return (params as CameraTestParams).instructionVideo !== void 0;
}

export type TestInfoResponse = {
  id: string;
  name: string;
  des: null | string;
  type_id: string;
  config_template_id: string;
  instructions: {
    content: string;
    icon: { name: string; color: string };
    id: string;
    index: number;
  }[];
  configs: [{ key: string; test_id: string; value: string }];
  params: CameraTestParams | AudioTestParams;
};

export async function getTestInfo(testId: string) {
  return await sendRequest<TestInfoResponse>(
    API_PATHS.services.testInfo({ testId }),
  ).then((info) => {
    const e = (s: string) => s.split("/").toReversed()[0];
    if (isCameraTestParams(info.params)) {
      info.params.instructionVideo = e(info.params.instructionVideo);
      info.params.beepAudio = e(info.params.beepAudio);
      info.params.audioInstruction = e(info.params.audioInstruction);
    } else {
      info.params.videoInstruction = e(info.params.videoInstruction);
    }
    return info;
  });
}
