import { API_PATHS } from "./routes";
import { f, jsonToFormData } from "./base";
import { appState } from "@/state";

// export async function getCandidateLoginUrl({
//   source,
//   externalId,
//   collectionId,
// }: {
//   source: string;
//   externalId: string;
//   collectionId: string;
// }) {
//   const url = API_PATHS.CandidateLoginUrl;
//   url.searchParams.append("source", source);
//   url.searchParams.append("external_id", externalId);
//   url.searchParams.append("collection_id", collectionId);
//   const r = await f(url, { method: "GET" });
//   const paramsText = await r.text();
//   const params = new URL(paramsText, "http://localhost").searchParams;
//   return { token: params.get("token") };
// }

export async function candidateToken({
  source,
  externalId,
  collectionId,
  token,
  platform = "web",
  redir = "",
  os = "",
  browser = "",
  extendedInfo = "",
}: {
  source: string;
  externalId: string;
  collectionId: string;
  token: string;
  platform?: string;
  redir?: string;
  os?: string;
  browser?: string;
  extendedInfo?: string;
}) {
  type ResponseJSON = { access_token: string; token_type: string };

  const url = API_PATHS.auths.candidateToken();
  const r = await f(url, {
    method: "POST",
    body: jsonToFormData({
      source,
      externalId,
      collectionId,
      token,
      platform,
      redir,
      os,
      browser,
      extendedInfo,
    }),
  });
  const { token_type, access_token }: ResponseJSON = await r.json();
  const authToken = { type: token_type, token: access_token };
  return authToken;
}
