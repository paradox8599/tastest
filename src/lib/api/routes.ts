import { API_SERVER_URL } from "../../vars";

function te({ key, v }: { key: string; v: any }) {
  throw new TypeError(`Unexpected type for param: ${key}:${v}`);
}

function path(
  path: string,
  params: { [key: string]: number | string | string[] } = {},
) {
  const query = Object.keys(params).map((key) => {
    const v = params[key];
    switch (typeof v) {
      case "number":
      case "string":
        return `${key}=${encodeURIComponent(v)}`;
      case "object":
        if (!Array.isArray(v)) te({ key, v });
        const vv = v.map((x) => encodeURIComponent(x)).join(",");
        return `${key}=${encodeURIComponent(vv)}`;
      default:
        te({ key, v });
    }
  });
  return new URL(
    `${path.replace(/^\//, "")}?${query.join("&")}`.replace(/\?$/, ""),
    API_SERVER_URL,
  );
}

export const API_PATHS = {
  // Actions
  actions: {
    consent: () => path(`actions/consent`),
    testResult: () => path(`actions/test-result`),
    skipTestSet: () => path(`actions/skip-test-set`),
    skipTest: () => path(`actions/skip-test`),
    repeatCollection: () => path(`actions/repeat-collection`),
    feedback: () => path(`actions/feedback`),
  },

  // Auths
  auths: {
    candidateToken: () => path("auths/candidate-token"),
  },

  // Services
  services: {
    state: () => path("services/state"),

    candidateLoginUrl: (params: {
      source: string;
      externalId: string;
      collectionId: string;
    }) => path(`services/candidate-login-url`, params),

    candidateLoginUrls: () => path("services/candidate-login-urls"),

    whoAmI: () => path("services/who-am-i"),

    isConsent: () => path("services/is-consent"),

    collection: () => path("services/collection"),

    process: () => path("services/process"),

    session: () => path("services/session"),

    route: () => path("services/route"),

    testRoute: ({ testId, set }: { testId: string; set: number }) =>
      path(`services/test-route/${testId}`, { set }),

    feedbackRoute: () => path("services/feedback-route"),

    hasFeedback: () => path("services/has-feedback"),

    isFeedbackEnabled: () => path("services/is-feedback-enabled"),

    testInfo: ({ testId }: { testId: string }) =>
      path(`services/test-info/${testId}`),

    testSetInfo: ({ testId }: { testId: string }) =>
      path(`services/test-set-info/${testId}`),

    testSetInstructions: ({ testSetId }: { testSetId: string }) =>
      path(`services/test-set-instructions/${testSetId}`),

    testInstructions: ({ testId }: { testId: string }) =>
      path(`services/test-instructions/${testId}`),

    testParams: ({ testId }: { testId: string }) =>
      path(`services/test-params/${testId}`),

    testFiles: ({ testId }: { testId: string }) =>
      path(`services/test-files/${testId}`),

    questionnaire: ({ questionnaireId }: { questionnaireId: string }) =>
      path(`services/questionnaire/${questionnaireId}`),

    currentAttempt: () => path("services/current-attempt"),

    isFinished: () => path("services/is-finished"),

    media: (alias: string) => path(`services/media/${alias}`),

    mediaTestFiles: ({ alias }: { alias: string }) =>
      path(`services/media/test-files/${alias}`),
  },
  questionnaires: {},
};
