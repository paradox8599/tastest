import { sendRequest } from "./base";
import { API_PATHS } from "./routes";

type QuestionGroup = {
  order: number;
  group_id: string;
  questionnaire_id: string;
};

export type QuestionOption = {
  des: string;
  extra_content?: string;
  group_id: string;
  order: number;
  required: boolean;
  title: string;
  type: string;
  with_extra_textarea: boolean;
  props: Record<string, any>;
};

type QuestionGroupWithQuestion = {
  id: string;
  des?: string;
  extra_content?: string;
  title?: string;
  visibility_script?: boolean;
  questions?: Array<QuestionOption>;
};

export type QuestionnaireResponse = {
  id: string;
  des?: string;
  title?: string;
  extra_content?: string;
  question_groups?: Array<QuestionGroup>;
  question_groups_with_questions?: Array<QuestionGroupWithQuestion>;
} | null;

/**
 * fetch questionnaire info
 * @param item_id questionnaireId
 * @returns
 */
export async function getQuestionnaire(item_id: string) {
  return await sendRequest<QuestionnaireResponse>(
    API_PATHS.services.questionnaire({ questionnaireId: item_id }),
  );
}

/**
 * save questionnaire result
 * @param params
 * @returns
 */
export async function saveQuestionnaire(params: Record<string, any>) {
  return await sendRequest<string>(API_PATHS.actions.feedback(), {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(params),
  });
}
