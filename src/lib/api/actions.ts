import { format } from "date-fns";
import { sendRequest } from "./base";
import { API_PATHS } from "./routes";
import { revalidateConsentState } from "@/hooks/useConsentState";

export type ConsentBody = {
  first_name: string;
  middle_name?: string;
  last_name: string;
  mobile?: string;
  email?: string;
  dob: Date;
  gender: "Male" | "Female" | "Other" | "Prefer not to say";
  extended_info?: {};
};

export async function consent(body: ConsentBody) {
  const r = await sendRequest<string>(API_PATHS.actions.consent(), {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      ...body,
      dob: format(body.dob, "yyyy-MM-dd"),
    }),
  });
  revalidateConsentState(r === "succeed");
  return r;
}

export type SkipTestBody = {
  test_set_id: number;
  test_id: string;
};

export async function skipTest(body: SkipTestBody) {
  const r = await sendRequest<string>(API_PATHS.actions.skipTest(), {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  });
  return r;
}

export type TestResultBody = {
  test_set_id: number;
  test_id: string;
  result?: {
    prepare_time: number;
    ready_time: number;
    start_time: number;
    end_time: number;
    record_attempt_num: number;
  };
  file?: Blob;
};

export async function submitTest(body: TestResultBody) {
  const data = new FormData();
  data.append(
    "json_str",
    JSON.stringify({
      test_set_id: body.test_set_id,
      test_id: body.test_id,
      result: body.result ?? {},
      file: undefined,
    }),
  );
  body.file && data.append("file", body.file);

  const r = await sendRequest<string>(API_PATHS.actions.testResult(), {
    method: "POST",
    body: data,
  });
  return r;
}
