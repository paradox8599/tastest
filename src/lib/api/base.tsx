import { appState } from "@/state";
import { toast } from "react-toastify";

export class HTTPError extends Error {
  status: number;
  text: string;
  constructor({ status, text }: { status: number; text: string }) {
    super();
    this.status = status;
    this.text = text;
  }
}

export class AuthError extends Error {
  constructor() {
    super();
  }
}

export function getAuth() {
  const auth = appState.authToken;
  if (!auth) throw new AuthError();
  return auth;
}

export function getAuthHeader() {
  const auth = getAuth();
  return {
    Authorization: `${auth.type} ${auth.token}`,
  };
}

export async function f(input: URL, init?: RequestInit) {
  try {
    const r = await fetch(input, init);
    if (!r.ok) {
      throw new HTTPError({ status: r.status, text: r.statusText });
    }
    return r;
  } catch (e) {
    if (e instanceof TypeError) {
      // no internet, wrong url/params, etc
      // TODO: i18n
      toast.error("No internet connection");
    } else if (e instanceof HTTPError) {
      // http error, 3xx/4xx/5xx
      // TODO: i18n
      if (e.status.toString().startsWith("5")) {
        toast.error("Something went wrong with server, please try again later");
      }
      if (e.status === 401) {
        appState.authToken = void 0;
      }
    } else {
      // TODO: i18n
      toast.error("Something went wrong");
    }
    console.error(e);
    throw e;
  }
}

export async function sendRequest<T>(url: URL, init: RequestInit = {}) {
  const r = await f(url, {
    ...init,
    headers: {
      ...(init?.headers ?? {}),
      ...getAuthHeader(),
    },
  });
  const val: T = await r.json();
  return val;
}

function camelCaseToSnakeCase(str: string) {
  return str
    .replace(/([A-Z])/g, (g) => `_${g[0].toLowerCase()}`)
    .replace(/^_/, "");
}

export function jsonToFormData(json: any) {
  const formData = new FormData();
  for (const key of Object.keys(json)) {
    formData.append(camelCaseToSnakeCase(key), json[key]);
  }
  return formData;
}
