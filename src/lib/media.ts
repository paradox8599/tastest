// TODO: setting constraints (resolution, height, widht)

export class Media {
  /**
   * Check if the browser supports getUserMedia
   */
  static canGetUserMedia() {
    return navigator.mediaDevices?.getUserMedia !== void 0;
  }

  /**
   * Check if the user has allowed access to the camera & audio
   */
  static async isAllowed() {
    const devices = await navigator.mediaDevices.enumerateDevices();
    return devices.some((d) => d.deviceId !== "");
  }

  /**
   * Check if the error is about media access permission
   */
  private static isPermissionError(e: any) {
    if (!(e instanceof DOMException)) return false;
    const notAllowed = ["Permission dismissed", "Permission denied"].includes(
      e.message,
    );
    return notAllowed;
  }

  static async requestVideoPermission() {
    await this.getUserMedia({ audio: null });
  }
  static async requestAudioPermission() {
    await this.getUserMedia({ video: null });
  }

  /**
   * Get a list of all available media devices
   */
  static async getDevices() {
    const devices = await navigator.mediaDevices.enumerateDevices();
    const audioDevices = devices.filter((d) => d.kind === "audioinput");
    const videoDevices = devices.filter((d) => d.kind === "videoinput");
    return { audioDevices, videoDevices };
  }

  /**
   * audio | video:
   *  - undefined: use default device
   *  - string: device id
   *  - null: do not use
   */
  static async getUserMedia({
    audio,
    video,
  }: {
    audio?: string | null;
    video?: string | null;
  }) {
    try {
      let videoSrc: false | { deviceId?: string; aspectRatio: number } = {
        aspectRatio: 1,
      };
      let audioSrc: false | { deviceId?: string } = {};
      // video
      if (video) videoSrc = { ...videoSrc, deviceId: video };
      else if (video === null) videoSrc = false;
      // audio
      if (audio) audioSrc = { deviceId: audio };
      else if (audio === null) audioSrc = false;

      const mediaStream = await navigator.mediaDevices.getUserMedia({
        video: videoSrc,
        audio: audioSrc,
      });
      return mediaStream;
    } catch (e) {
      if (this.isPermissionError(e)) return undefined;
      throw e;
    }
  }
}

export async function blobToBase64(blob: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => {
      if (reader.result) {
        resolve(reader.result as string);
      } else {
        reject(new Error("Failed to read blob"));
      }
    };
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
}
