"use client";
import React from "react";
import { useAuth } from "@/hooks/useAuth";
import { useConsentCheck } from "@/hooks/useConsentCheck";
import { useConsentState } from "@/hooks/useConsentState";
import { useInitToken } from "@/hooks/useInitToken";
import { usePrepare } from "@/hooks/usePrepare";
import { usePath } from "@/hooks/usePath";
import { AppPathList, AppPaths } from "@/route";

export function AuthProvider({ children }: { children: React.ReactNode }) {
  useInitToken();
  useAuth();
  useConsentCheck();
  usePrepare();
  const { isConsent } = useConsentState();

  const path = usePath();
  // check if can load page without consent
  const canLoad =
    isConsent !== void 0 &&
    (isConsent === true ||
      (isConsent === false &&
        AppPathList.indexOf(path) <= AppPathList.indexOf(AppPaths.consent)));

  // TODO: Auth check and redirect to home if not authed

  // TODO: display loading state before {isConsent} is ready
  return <>{canLoad && children}</>;
}
