"use client";
import { useTestInfo } from "@/hooks/useTestInfo";
import React from "react";

export function DataProvider({ children }: { children: React.ReactNode }) {
  useTestInfo();
  return <>{children}</>;
}
