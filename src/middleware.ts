import createMiddleware from "next-intl/middleware";

export default createMiddleware({
  localePrefix: "as-needed",
  // A list of all locales that are supported
  locales: ["en", "de", "zh"],

  // Used when no locale matches
  defaultLocale: "en",
});

export const config = {
  matcher: [
    "/",
    "/(de|en|zh)/:path*",
    "/((?!api|_next|_vercel|favicon|.*\\..*).*)",
  ],
};
