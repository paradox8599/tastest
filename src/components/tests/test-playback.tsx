"use client";

import { useTranslations } from "next-intl";
import Image from "next/image";
import Card from "@/components/questionnaire/card";
import { MdFilledButton } from "@/components/md";
import { tw } from "@/style";
import I from "@/components/icon";

export default function TestPlayback({
  children,
  onNext,
  onRetry,
}: {
  children?: React.ReactNode;
  onNext?: () => void;
  onRetry?: () => void;
}) {
  const tr = useTranslations("wellDonePage");

  return (
    <main className="w-full py-2 px-8 flex flex-col gap-2">
      <div className="flex-center">{children}</div>
      <div
        className={tw(
          "bg-white",
          "border-double border-4 border-[#FED130]",
          "flex flex-col items-center",
          "py-2",
          "animate-border-jump",
        )}
      >
        <div className="text-2xl font-bold text-[#585DDA] text-center">
          {tr("wellDone.title")}
        </div>
        <div className="text-base font-normal text-[#636366] whitespace-pre-wrap text-center">
          {tr("wellDone.content")}
        </div>
        <Image src="/trophy.svg" alt="trophy" width={83} height={62} />
      </div>
      <Card className="py-6 flex flex-col gap-2">
        <div className="font-normal text-base text-center">
          {tr("next.content")}
        </div>
        <MdFilledButton
          className={tw("w-full", "[--md-sys-color-primary:#585DDA]")}
          onClick={onNext}
        >
          {tr("next.label")}
        </MdFilledButton>
        <div className="font-normal text-base text-center">
          {tr("again.content")}
        </div>
        <MdFilledButton
          className={tw("w-full", "[--md-sys-color-primary:#B80000]")}
          onClick={onRetry}
        >
          <div className="flex-center gap-2">
            <span>{tr("again.label")}</span>
            <I className="text-base">replay</I>
          </div>
        </MdFilledButton>
      </Card>
    </main>
  );
}
