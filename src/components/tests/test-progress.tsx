import { tw } from "@/style";
import TrophySlot from "./trophy-slot";
import React from "react";

export default function TestProgress({
  total,
  current,
}: {
  total: number;
  current: number;
}) {
  return (
    <div className="w-full flex-center">
      {Array.from({ length: total }, (_el, index) => (
        <React.Fragment key={index}>
          <TrophySlot
            done={index + 1 < current}
            current={index + 1 === current}
            index={index + 1}
          />
          {index + 1 < total && (
            <div
              className={tw(
                "flex-1 h-0.5",
                index + 1 < current ? "bg-yellow-400" : "bg-gray-300",
              )}
            ></div>
          )}
        </React.Fragment>
      ))}
    </div>
  );
}
