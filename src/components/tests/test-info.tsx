import Card from "@/components/questionnaire/card";
import { useTranslations } from "next-intl";
import { MdFilledButton } from "@/components/md";
import Image from "next/image";
import LargeTrhphySVG from "@/../public/large-trophy.svg";
import TestProgress from "./test-progress";

export default function TestInfo({
  total,
  title,
  instructions,
  onNext,
}: {
  total: number;
  title: string;
  instructions: string[];
  onNext?: () => void;
}) {
  const tr = useTranslations("testsPage");

  return (
    <main className="p-4 flex flex-col gap-4">
      <p className="font-semibold text-3xl text-center">{title}</p>

      <Card className="px-2 smd:px-4 py-2 flex flex-col items-center gap-4">
        <Image
          src={LargeTrhphySVG}
          alt="test trophy icon"
          className="w-1/2 max-w-44"
        />
        <TestProgress total={total} current={0} />
        <div className="text-md whitespace-pre-wrap py-2 smd:py-6">
          {instructions.map((inst, index) => (
            <p
              key={index}
              className="rounded shadow my-2 p-2 bg-surfaceContainer"
            >
              {inst}
            </p>
          ))}
        </div>
      </Card>

      <div className="text-center sticky bottom-0 py-2">
        <MdFilledButton className="px-16" onClick={onNext}>
          {tr("buttons.next")}
        </MdFilledButton>
      </div>
    </main>
  );
}
