import { tw } from "@/style";
import Image from "next/image";

export default function TrophySlot({
  done = false,
  current = false,
  index,
}: {
  done?: boolean;
  current?: boolean;
  index: number;
}) {
  return (
    <div
      className={tw(
        "w-8",
        "smd:w-10",
        "aspect-square",
        "rounded-full",
        "flex-center",
        "border-2",
        done || current
          ? "bg-white text-black border-yellow-400"
          : "bg-gray-300 text-white",
      )}
    >
      {done ? (
        <Image src="/trophy.svg" alt="trophy" width={24} height={24} />
      ) : (
        <span>{index}</span>
      )}
    </div>
  );
}
