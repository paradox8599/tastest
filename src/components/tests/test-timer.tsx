import React from "react";
import { MdLinearProgress } from "../md";
import { tw } from "@/style";

export function TestTimer({ durationMs }: { durationMs: number }) {
  const [timeLeft, setTimeLeft] = React.useState(durationMs);

  React.useEffect(() => {
    const step = 50; // ms
    let timer: Timer;
    timer = setInterval(() => {
      if (timeLeft < 0) return clearInterval(timer);
      setTimeLeft((t) => t - step);
    }, step);

    return () => clearInterval(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <MdLinearProgress
        value={timeLeft}
        max={durationMs}
        buffer={0}
        className={tw(
          "[--md-linear-progress-track-height:10px]",
          "[--md-linear-progress-track-shape:10px]",
          "[--md-linear-progress-active-indicator-height:10px]",
        )}
      />
    </>
  );
}
