import { tw } from "@/style";

export function RecordingIndicator({
  show = true,
  children,
  className = "",
}: {
  show?: boolean;
  className?: string;
  children: React.ReactNode;
}) {
  return (
    <div className="relative">
      <div
        className={tw(
          "border-4",
          "rounded-lg",
          "w-full",
          show ? "border-red-700" : "",
          className,
        )}
      >
        {children}
      </div>
      {show && (
        <div
          className={tw(
            "absolute top-0 left-0",
            "m-2 px-2 py-1",
            "rounded-lg",
            "bg-red-700 text-white",
            "text-sm",
            "flex flex-center gap-1",
          )}
        >
          <div className="rounded-full bg-white w-3 h-3" />
          Recording
        </div>
      )}
    </div>
  );
}
