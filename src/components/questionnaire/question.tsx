import {
  MdRadio,
  MdCheckbox,
  MdSlider,
  MdFilledTextField,
  MdOutlinedSelect,
  MdSelectOption,
} from "@/components/md";
import { QuestionOption } from "@/lib/api/questionnaire";
import { FormEventHandler } from "react";
import { useTranslations } from "next-intl";
import I from "@/components/icon";
import { tw } from "@/style";

export enum QuestionType {
  Rate = "Rate",
  Radio = "Radio",
  Select = "Select",
  Slider = "Slider",
  Checkbox = "Checkbox",
  Textarea = "Textarea",
  InputText = "InputText",
  InputNumber = "InputNumber",
}

export default function Question({
  title,
  type,
  order,
  props,
  required,
  change,
  blur,
  values,
}: QuestionOption & {
  change: FormEventHandler;
  blur: FormEventHandler;
  values: Record<string, any>;
}) {
  const tr = useTranslations("questionnairePage.question");

  let questionForm = null;

  switch (type) {
    case QuestionType.Rate:
      questionForm = <div className="text-center">{tr("unsupport")}</div>;
      break;
    case QuestionType.Radio:
      questionForm = (
        <div
          role="radiogroup"
          aria-label={props.placeholder}
          className="flex flex-col gap-2"
        >
          {props.options.map(
            (el: { label: string; value: string }, index: number) => (
              <label key={index} className="inline-flex gap-1 px-1">
                <MdRadio
                  name={order.toString()}
                  value={el.value}
                  aria-label={el.label}
                  onInput={change}
                />
                {el.label}
              </label>
            ),
          )}
        </div>
      );
      break;
    case QuestionType.Select:
      questionForm = (
        <MdOutlinedSelect
          name={order.toString()}
          label={props.placeholder}
          onInput={change}
          required={required}
        >
          {props.options.map(
            (el: { label: string; value: string }, index: number) => (
              <MdSelectOption key={index} value={el.value}>
                <div slot="headline">{el.label}</div>
              </MdSelectOption>
            ),
          )}
        </MdOutlinedSelect>
      );
      break;
    case QuestionType.Slider:
      questionForm = (
        <div className="flex items-start">
          <I
            className={tw(
              "material-symbols-outlined text-3xl",
              values[order] &&
                values[order] <
                  props.range.min + (props.range.max - props.range.min) / 2
                ? "text-[#B80000]"
                : "",
            )}
          >
            sentiment_dissatisfied
          </I>
          <div className="flex-1">
            <MdSlider
              className={tw(
                "w-full",
                "[--md-slider-handle-color:transparent]",
                "[--md-slider-handle-shadow-color:transparent]",
                "[--md-slider-focus-handle-color:#5DB075]",
                "[--md-slider-hover-handle-color:#5DB075]",
                "[--md-slider-pressed-handle-color:#5DB075]",
                "[--md-slider-active-track-color:#5DB075]",
                "[--md-slider-inactive-track-color:#BDBDBD]",
              )}
              name={order}
              value={values[order] || props.range.min}
              {...props.range}
              ticks
              onInput={change}
            />
            <div className="w-full flex justify-between text-xs text-black px-4">
              {Array.from({ length: props.range.max }, (_el, index) => (
                <span key={`span${index + 1}`}>{index + 1}</span>
              ))}
            </div>
          </div>
          <I
            className={tw(
              "material-symbols-outlined text-3xl",
              values[order] &&
                values[order] >
                  props.range.min + (props.range.max - props.range.min) / 2
                ? "text-[#5DB075]"
                : "",
            )}
          >
            sentiment_satisfied
          </I>
        </div>
      );
      break;
    case QuestionType.Checkbox:
      questionForm = (
        <div
          role="group"
          aria-label={props.placeholder}
          className="flex flex-col gap-2"
        >
          {props.options.map(
            (el: { label: string; value: string }, index: number) => (
              <label key={index} className="inline-flex gap-1 px-1">
                <MdCheckbox
                  name={order.toString()}
                  value={el.value}
                  aria-label={el.label}
                  onInput={change}
                />
                {el.label}
              </label>
            ),
          )}
        </div>
      );
      break;
    case QuestionType.Textarea:
      questionForm = (
        <MdFilledTextField
          type="textarea"
          required={required}
          name={order.toString()}
          label={props.placeholder}
          onInput={change}
          onBlur={blur}
        />
      );
      break;
    case QuestionType.InputText:
      questionForm = (
        <MdFilledTextField
          required={required}
          name={order.toString()}
          label={props.placeholder}
          onInput={change}
          onBlur={blur}
        />
      );
      break;
    case QuestionType.InputNumber:
      questionForm = (
        <MdFilledTextField
          type="number"
          required={required}
          name={order.toString()}
          label={props.placeholder}
          onInput={change}
          onBlur={blur}
        />
      );
      break;
  }

  return (
    <div className="flex flex-col gap-3">
      <p className="font-medium text-lg text-black">{title}</p>
      {questionForm}
    </div>
  );
}
