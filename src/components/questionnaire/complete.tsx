import { useTranslations } from "next-intl";
import { useFormikContext } from "formik";
import Image from "next/image";
import { MdFilledButton } from "@/components/md";
import { tw } from "@/style";
import { logout } from "@/state";

export default function Complete() {
  const trComplete = useTranslations("questionnairePage.complete");
  const { values } = useFormikContext();

  return (
    <div className="flex flex-col gap-4 bg-white px-3 py-4 rounded-lg justify-center">
      <p className="text-4xl leading-[48px] text-black font-bold text-center whitespace-pre-wrap">
        {trComplete.rich("content", {
          tap: (tap) => (
            <span className="font-semibold text-[#002060]">{tap}</span>
          ),
          talk: (talk) => (
            <span className="font-semibold text-[#0070C0]">{talk}</span>
          ),
        })}
      </p>
      <div className="flex flex-wrap gap-6 justify-center content-center">
        {Array.from(
          { length: values ? Object.keys(values as Object).length : 0 },
          (_el, index) => (
            <Image
              key={index}
              src="/trophy.svg"
              alt="trophy"
              width={72}
              height={72}
            />
          ),
        )}
      </div>
      <div className="text-center">
        <MdFilledButton
          className={tw(
            "[--md-filled-button-leading-space:60px]",
            "[--md-filled-button-trailing-space:60px]",
            "[--md-sys-color-primary:#B80000]",
            "[--md-sys-color-on-primary:#ffffff]",
          )}
          onClick={logout}
        >
          {trComplete("logout")}
        </MdFilledButton>
      </div>
    </div>
  );
}
