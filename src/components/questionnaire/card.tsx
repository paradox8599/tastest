import { PropsWithChildren } from "react";
import { tw } from "@/style";

export default function Card({
  children,
  className,
}: PropsWithChildren & { className?: string }) {
  return (
    <div className={tw("bg-white px-3 py-4 rounded-lg", className || "")}>
      {children}
    </div>
  );
}
