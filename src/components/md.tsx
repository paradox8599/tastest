import React from "react";
import { createComponent } from "@lit/react";
import { MdFilledButton as mdFilledButton } from "@material/web/button/filled-button";
import { MdElevatedButton as mdElevatedButton } from "@material/web/button/elevated-button";
import { MdFilledTonalButton as mdFilledTonalButton } from "@material/web/button/filled-tonal-button";
import { MdOutlinedButton as mdOutlinedButton } from "@material/web/button/outlined-button";
import { MdTextButton as mdTextButton } from "@material/web/button/text-button";

import { MdFilledTextField as mdFilledTextField } from "@material/web/textfield/filled-text-field";
import { MdOutlinedTextField as mdOutlinedTextField } from "@material/web/textfield/outlined-text-field";

import { MdCircularProgress as mdCircularProgress } from "@material/web/progress/circular-progress";
import { MdLinearProgress as mdLinearProgress } from "@material/web/progress/linear-progress";
import { MdRipple as mdRipple } from "@material/web/ripple/ripple";

import { MdFilledSelect as mdFilledSelect } from "@material/web/select/filled-select";
import { MdOutlinedSelect as mdOutlinedSelect } from "@material/web/select/outlined-select";
import { MdSelectOption as mdSelectOption } from "@material/web/select/select-option";

import { MdDialog as mdDialog } from "@material/web/dialog/dialog";

import { MdMenu as mdMenu } from "@material/web/menu/menu";
import { MdSubMenu as mdSubMenu } from "@material/web/menu/sub-menu";
import { MdMenuItem as mdMenuItem } from "@material/web/menu/menu-item";

import { MdRadio as mradio } from "@material/web/radio/radio";
import { MdIcon as mdicon } from "@material/web/icon/icon";

import { MdChipSet as mdChipSet } from "@material/web/chips/chip-set";
import { MdInputChip as mdInputChip } from "@material/web/chips/input-chip";
import { MdFilterChip as mdFilterChip } from "@material/web/chips/filter-chip";
import { MdAssistChip as mdAssistChip } from "@material/web/chips/assist-chip";
import { MdSuggestionChip as mdSuggestionChip } from "@material/web/chips/suggestion-chip";

import { MdCheckbox as mdCheckbox } from "@material/web/checkbox/checkbox";

import { MdSlider as mdSlider } from "@material/web/slider/slider";

export const MdElevatedButton = createComponent({
  tagName: "md-elevated-button",
  react: React,
  elementClass: mdElevatedButton,
});

export const MdFilledButton = createComponent({
  tagName: "md-filled-button",
  react: React,
  elementClass: mdFilledButton,
});

export const MdFilledTonalButton = createComponent({
  tagName: "md-filled-tonal-button",
  react: React,
  elementClass: mdFilledTonalButton,
});

export const MdOutlinedButton = createComponent({
  tagName: "md-outlined-button",
  react: React,
  elementClass: mdOutlinedButton,
});

export const MdTextButton = createComponent({
  tagName: "md-text-button",
  react: React,
  elementClass: mdTextButton,
});

export const MdFilledTextField = createComponent({
  tagName: "md-filled-text-field",
  react: React,
  elementClass: mdFilledTextField,
});

export const MdOutlinedTextField = createComponent({
  tagName: "md-outlined-text-field",
  react: React,
  elementClass: mdOutlinedTextField,
});

export const MdCircularProgress = createComponent({
  tagName: "md-circular-progress",
  react: React,
  elementClass: mdCircularProgress,
});

export const MdLinearProgress = createComponent({
  tagName: "md-linear-progress",
  react: React,
  elementClass: mdLinearProgress,
});

export const MdRipple = createComponent({
  tagName: "md-ripple",
  react: React,
  elementClass: mdRipple,
});

export const MdFilledSelect = createComponent({
  tagName: "md-filled-select",
  react: React,
  elementClass: mdFilledSelect,
});

export const MdOutlinedSelect = createComponent({
  tagName: "md-outlined-select",
  react: React,
  elementClass: mdOutlinedSelect,
});

export const MdSelectOption = createComponent({
  tagName: "md-select-option",
  react: React,
  elementClass: mdSelectOption,
});

export const MdDialog = createComponent({
  tagName: "md-dialog",
  react: React,
  elementClass: mdDialog,
});

export const MdMenu = createComponent({
  tagName: "md-menu",
  react: React,
  elementClass: mdMenu,
});

export const MdSubMenu = createComponent({
  tagName: "md-sub-menu",
  react: React,
  elementClass: mdSubMenu,
});

export const MdMenuItem = createComponent({
  tagName: "md-menu-item",
  react: React,
  elementClass: mdMenuItem,
});

export const MdChipSet = createComponent({
  tagName: "md-chip-set",
  react: React,
  elementClass: mdChipSet,
});

export const MdInputChip = createComponent({
  tagName: "md-input-chip",
  react: React,
  elementClass: mdInputChip,
});

export const MdFilterChip = createComponent({
  tagName: "md-filter-chip",
  react: React,
  elementClass: mdFilterChip,
});

export const MdAssistChip = createComponent({
  tagName: "md-assist-chip",
  react: React,
  elementClass: mdAssistChip,
});

export const MdSuggestionChip = createComponent({
  tagName: "md-suggestion-chip",
  react: React,
  elementClass: mdSuggestionChip,
});

export const MdRadio = createComponent({
  tagName: "md-radio",
  react: React,
  elementClass: mradio,
});

export const MdIcon = createComponent({
  tagName: "md-icon",
  react: React,
  elementClass: mdicon,
});

export const MdCheckbox = createComponent({
  tagName: "md-checkbox",
  react: React,
  elementClass: mdCheckbox,
});

export const MdSlider = createComponent({
  tagName: "md-slider",
  react: React,
  elementClass: mdSlider,
});
