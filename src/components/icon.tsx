export default function I({
  className = "",
  children,
}: {
  className?: string;
  children: string;
}) {
  return (
    <span className={`material-symbols-rounded ${className}`}>{children}</span>
  );
}
