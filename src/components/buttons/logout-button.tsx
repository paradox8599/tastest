import { logout } from "@/state";
import { MdFilledButton } from "../md";
import { useTranslations } from "next-intl";

export function LogoutButton() {
  const tr = useTranslations("components.buttons");
  return (
    <MdFilledButton onClick={() => logout()}>{tr("logout")}</MdFilledButton>
  );
}
