"use client";
import React from "react";
import { tw } from "@/style";
import { RecordingIndicator } from "./tests/recording-frame";

export function CameraPreview({
  videoStream,
  mirrored = true,
  recording = false,
}: {
  videoStream?: MediaStream;
  mirrored?: boolean;
  recording?: boolean;
}) {
  const vRef = React.useRef<HTMLVideoElement>(null);

  React.useEffect(() => {
    if (vRef.current) vRef.current.srcObject = videoStream ?? null;
  }, [videoStream]);

  return (
    <RecordingIndicator show={recording} className="aspect-square">
      <video
        muted
        autoPlay
        playsInline
        ref={vRef}
        className={tw(
          "rounded",
          "w-full",
          "aspect-square",
          mirrored ? "[transform:rotateY(180deg)]" : "",
        )}
      />
    </RecordingIndicator>
  );
}
