import { CountdownCircleTimer } from "react-countdown-circle-timer";

export type CircleTimerProps = {
  duration: number;
  size?: number;
  onComplete?: () => void;
};

export default function CircleTimer({
  duration,
  size = 160,
  onComplete,
}: CircleTimerProps) {
  return (
    <CountdownCircleTimer
      isPlaying
      duration={duration}
      colors={["#5DB075", "#5DB075"]}
      colorsTime={[1, 0]}
      size={size}
      onComplete={onComplete}
    ></CountdownCircleTimer>
  );
}
