import CircleTimer, { CircleTimerProps } from "./circle-timer";
import Card from "@/components/questionnaire/card";

export type CountDownProps = { title: string; text: string } & CircleTimerProps;

export default function CountDown({
  title,
  duration,
  text,
  onComplete,
}: CountDownProps) {
  return (
    <>
      <p className="text-3xl font-semibold text-black leading-9 text-center whitespace-pre-wrap py-5">
        {title}
      </p>
      <div className="w-full py-3 bg-[#585DDA] rounded-lg flex items-center justify-center">
        <CircleTimer duration={duration} onComplete={onComplete}></CircleTimer>
      </div>
      <Card className="py-7">
        <p className="text-3xl font-semibold text-black leading-10 text-center whitespace-pre-wrap">
          {text}
        </p>
      </Card>
    </>
  );
}
