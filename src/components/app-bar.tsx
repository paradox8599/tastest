"use client";

import React from "react";
import Image from "next/image";
import { useTranslations } from "next-intl";
import { API_PATHS } from "@/lib/api/routes";
import { appState } from "@/state";
import { useSnapshot } from "valtio";
import I from "./icon";

import { MdMenu, MdMenuItem, MdTextButton } from "./md";
import { MdTextButton as mtb } from "@material/web/button/text-button";
import { MdMenu as mm } from "@material/web/menu/menu";
import { useRouter, useSearchParams } from "next/navigation";
import { localeNames, locales } from "@/i18n";
import { usePath } from "@/hooks/usePath";
import { ASSETS } from "@/vars";
import { useAuthState } from "@/hooks/useAuthState";
import { tw } from "@/style";

function LocaleMenu() {
  const path = usePath();
  const searchParams = useSearchParams();
  const router = useRouter();
  const tr = useTranslations();

  const [menuOpen, setMenuOpen] = React.useState(false);

  const menuRef = React.useRef<mm>(null);
  const anchorRef = React.useRef<mtb>(null);

  // set menuOpen back to false when the menu is closed by other actions (not by clicking the menu button)
  React.useEffect(() => {
    const onClosed = () => {
      if (menuOpen) setMenuOpen(false);
    };
    const menu = menuRef.current;
    menu?.addEventListener("closed", onClosed);

    return () => {
      menu?.removeEventListener("closed", onClosed);
    };
  }, [menuOpen]);

  return (
    <div className="relative">
      <MdTextButton
        ref={anchorRef}
        onClick={() => setMenuOpen((prev) => !prev)}
      >
        <div className="flex-center gap-1">
          <I>translate</I>
          <span>{tr("locale")}</span>
          <I className="mx-[-0.5rem]">arrow_drop_down</I>
        </div>
      </MdTextButton>

      <MdMenu
        ref={menuRef}
        open={menuOpen}
        positioning="popover"
        anchorElement={anchorRef.current}
      >
        {locales.map((locale) => (
          <MdMenuItem
            key={locale}
            onClick={() => {
              setMenuOpen(false);
              const url = `/${locale}${path}?${searchParams.toString()}`;
              router.replace(url);
            }}
          >
            <div slot="headline">
              {localeNames[locale as keyof typeof localeNames]}
            </div>
          </MdMenuItem>
        ))}
      </MdMenu>
    </div>
  );
}

export default function AppBar({ className }: { className?: string }) {
  const { initToken } = useSnapshot(appState);
  const { authed } = useAuthState();
  return (
    <div className={className}>
      <div
        className={tw(
          "relative",
          "shadow-sm",
          "bg-surfaceContainer",
          "flex",
          "items-center",
          "justify-between",
          "p-1",
          "gap-1",
        )}
      >
        {/* UTAS logo */}
        <div>
          <Image
            src={API_PATHS.services.media(ASSETS.images.utasLogo).href}
            priority
            alt="UTAS logo"
            width={30}
            height={30}
            className="w-auto h-auto"
          />
        </div>
        {/* user info */}
        {authed && (
          <div className="absolute inset-0 flex-center text-onSurface">
            <I>person</I>
            <div>{initToken?.external_id}</div>
          </div>
        )}
        {/* locale select */}
        <LocaleMenu />
      </div>
    </div>
  );
}
