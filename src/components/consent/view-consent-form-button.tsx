import { tw } from "@/style";
import I from "../icon";
import { MdRipple } from "@/components/md";

export function ViewConsentFormButton({ text }: { text: string }) {
  return (
    <button
      className={tw(
        "w-full",
        "relative",
        "px-2",
        "my-2",
        "flex-center",
        "gap-2",
        "bg-primary",
        "text-onPrimary",
        "min-h-12",
        "rounded-lg",
      )}
      onClick={() => console.log("clicked")}
    >
      <I>description</I>
      <span>{text}</span>
      <I>open_in_new</I>
      <MdRipple></MdRipple>
    </button>
  );
}
