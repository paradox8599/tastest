"use client";

import React from "react";
import { MdOutlinedTextField, MdIcon } from "@/components/md";
import I from "@/components/icon";
import { format } from "date-fns";

export default function CalendarInput({
  value,
  onDayPick,
  label,
  name,
}: {
  value?: Date;
  onDayPick?: (date?: Date) => void;
  label?: string;
  name?: string;
}) {
  return (
    <>
      <MdOutlinedTextField
        required
        name={name}
        type="date"
        hasLeadingIcon
        label={label}
        value={value ? format(value, "yyyy-MM-dd") : ""}
        className="h-[58px]"
        onInput={(e) =>
          onDayPick?.((e.target as HTMLInputElement).valueAsDate ?? void 0)
        }
      >
        <MdIcon slot="leading-icon">
          <I>calendar_today</I>
        </MdIcon>
      </MdOutlinedTextField>
    </>
  );
}
