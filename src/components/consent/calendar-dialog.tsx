// "use client";
//
// import React from "react";
// import { MdFilledButton, MdDialog, MdTextButton } from "@/components/md";
// import { DayPicker } from "react-day-picker";
// import "react-day-picker/dist/style.css";
// import { MdDialog as mdialog } from "@material/web/dialog/dialog";
// import { CONSENT_AGE } from "@/vars";
//
// export default function CanlendarDialog({
//   value,
//   open = false,
//   onFinish,
//   onClose,
// }: {
//   value?: Date;
//   open: boolean;
//   onFinish?: (date?: Date) => void;
//   onClose?: () => void;
// }) {
//   const dRef = React.useRef<mdialog>(null);
//   const [date, setDate] = React.useState<Date | undefined>(value);
//
//   React.useEffect(() => {
//     if (open) setDate(value);
//   }, [open, value]);
//
//   return (
//     <MdDialog ref={dRef} open={open} type="alert">
//       <div slot="content" className="text-start">
//         <DayPicker
//           styles={{
//             day: { width: "30px", height: "30px" },
//           }}
//           mode="single"
//           captionLayout="dropdown"
//           fromYear={new Date().getFullYear() - 120}
//           toYear={new Date().getFullYear() - CONSENT_AGE}
//           showOutsideDays
//           selected={date}
//           onSelect={setDate}
//         />
//       </div>
//       <div slot="actions">
//         <MdTextButton
//           type="button"
//           onClick={() => {
//             onClose?.();
//           }}
//         >
//           Cancel
//         </MdTextButton>
//         <MdFilledButton
//           type="button"
//           onClick={() => {
//             onFinish?.(date);
//             onClose?.();
//           }}
//         >
//           Select
//         </MdFilledButton>
//       </div>
//     </MdDialog>
//   );
// }
