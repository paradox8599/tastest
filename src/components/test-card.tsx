import {
  getCollection,
  getProcess,
  getRoute,
  getTestInfo,
  isConsent,
} from "@/lib/api/services";
import { MdFilledButton } from "./md";
import TestProgress from "./tests/test-progress";
import { useState } from "react";

export default function TestCard() {
  let [current, setCurrent] = useState(0);
  const total = 5;

  return (
    <div className="flex-col flex-center gap-2">
      {/* consent */}
      <MdFilledButton
        onClick={async () => {
          const c = await isConsent();
          console.log(c);
        }}
      >
        Get Consent
      </MdFilledButton>

      {/* collection */}
      <MdFilledButton
        onClick={async () => {
          const col = await getCollection();
          console.log(col);
        }}
      >
        Get Collection
      </MdFilledButton>

      {/* process */}
      <MdFilledButton
        onClick={async () => {
          const proc = await getProcess();
          console.log(proc);
        }}
      >
        Get Process
      </MdFilledButton>

      {/* route */}
      <MdFilledButton
        onClick={async () => {
          const rt = await getRoute();
          console.log(rt);
        }}
      >
        Get Route
      </MdFilledButton>

      {/* test info */}
      <MdFilledButton
        onClick={async () => {
          const ti = await getTestInfo("1mobile1");
          console.log(ti);
        }}
      >
        Get TestInfo
      </MdFilledButton>
      <div className="w-60">
        <TestProgress total={total} current={current}></TestProgress>
      </div>
      <MdFilledButton
        onClick={() => setCurrent(current > total ? 1 : current + 1)}
      >
        Next Step
      </MdFilledButton>
    </div>
  );
}
