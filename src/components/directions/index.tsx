import Card from "@/components/questionnaire/card";
import { useTranslations } from "next-intl";

export type DirectionsProps = {
  total: number;
};

export default function Directions({ total }: DirectionsProps) {
  const tr = useTranslations("directionsPage");

  return (
    <Card className="px-11 py-11 flex flex-col gap-14">
      <p className="font-semibold text-black text-3xl leading-normal text-center whitespace-pre-wrap">
        {tr("title", { total })}
      </p>
      <p className="font-medium text-[#636366] text-xl text-center whitespace-pre-wrap">
        {tr("content")}
      </p>
    </Card>
  );
}
