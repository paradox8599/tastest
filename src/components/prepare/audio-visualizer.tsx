"use client";
import React from "react";
import { useVoiceVisualizer, VoiceVisualizer } from "react-voice-visualizer";

export default function AudioVisualizer() {
  const controls = useVoiceVisualizer();
  // const { error } = controls;

  // Get the recorded audio blob
  React.useEffect(() => {
    controls.startRecording();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Get the error when it occurs
  // React.useEffect(() => {
  //   if (!error) return;
  //   console.error(error);
  // }, [error]);

  return (
    <VoiceVisualizer
      controls={controls}
      isControlPanelShown={false}
      barWidth={4}
      height={150}
      rounded={5}
      mainBarColor="#888"
      secondaryBarColor="#888"
      defaultAudioWaveIconColor="#888"
      defaultMicrophoneIconColor="#888"
    />
  );
}
