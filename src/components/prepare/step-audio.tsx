"use client";
import React from "react";
import { MdFilledSelect, MdSelectOption } from "../md";
import { appState } from "@/state";
import dynamic from "next/dynamic";
import { useMediaDevices } from "@/hooks/useMediaDevices";

// Lazy load audio-visualizer on client side as it is client only
// errors will occur when rendered on server
const AudioVisualizer = dynamic(() => import("./audio-visualizer"), {
  ssr: false,
});

export function InstructionAudio({ tr }: { tr: any }) {
  const { audioDevices, audioDeviceId } = useMediaDevices();

  function assignAudio(deviceId?: string) {
    appState.media.audioDeviceId = deviceId ?? appState.media.audioDeviceId;
  }

  React.useEffect(() => {
    assignAudio(audioDevices?.[0]?.deviceId);
  }, [audioDevices]);

  return (
    <div>
      <p className="text-start">{tr("inst.i3")}</p>
      <div>
        <MdFilledSelect
          value={audioDeviceId}
          onInput={(e) => {
            const select = e.target as HTMLSelectElement;
            assignAudio(select.value);
          }}
        >
          {audioDevices?.map((d, i) => (
            <MdSelectOption key={i} value={d.deviceId}>
              <div slot="headline">{d.label}</div>
            </MdSelectOption>
          ))}
        </MdFilledSelect>

        <AudioVisualizer />
      </div>
    </div>
  );
}
