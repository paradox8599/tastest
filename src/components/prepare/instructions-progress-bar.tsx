import { tw } from "@/style";
import { MdLinearProgress } from "../md";

export function InstructionProgressBar({
  value,
  steps,
  className = "",
  barColor = "bg-white",
}: {
  value: number;
  steps: number;
  className?: string;
  barColor?: string;
}) {
  return (
    <div className={className}>
      <div className="relative">
        <MdLinearProgress
          value={value}
          max={steps}
          className={tw(
            "[--md-linear-progress-track-height:10px]",
            "[--md-linear-progress-track-shape:10px]",
            "[--md-linear-progress-active-indicator-height:10px]",
          )}
        />
        <div className="absolute inset-0 flex flex-row justify-evenly">
          {Array.from({ length: steps - 1 }).map((_, i) => (
            <div key={i} className={`w-1 ${barColor}`}></div>
          ))}
        </div>
      </div>
    </div>
  );
}
