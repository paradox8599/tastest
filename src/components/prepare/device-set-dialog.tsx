import React from "react";
import { useTranslations } from "next-intl";
import { MdDialog, MdFilledButton } from "../md";
import type { MdDialog as mdialog } from "@material/web/dialog/dialog";
import I from "../icon";

export function DeviceSetDialog({
  open = false,
  device,
  onClose,
}: {
  open: boolean;
  device: string;
  onClose?: () => void;
}) {
  const tr = useTranslations("preparePage.dialog.wellDone");
  const dRef = React.useRef<mdialog>(null);

  return (
    <MdDialog ref={dRef} open={open} type="alert">
      <div slot="headline" className="flex-center">
        {tr("title")}
      </div>
      <div slot="content" className="flex-center flex-col gap-2">
        <div className="text-start">{tr("content", { device })}</div>
        <I className="text-8xl text-green-700">check_circle</I>
      </div>
      <div slot="actions" className="flex-center">
        <MdFilledButton
          className="w-full"
          onClick={() => {
            dRef.current?.close();
            onClose?.();
          }}
        >
          {tr("confirm")}
        </MdFilledButton>
      </div>
    </MdDialog>
  );
}
