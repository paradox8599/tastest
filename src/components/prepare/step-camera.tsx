import React from "react";
import { MdFilledSelect, MdSelectOption } from "../md";
import { appState } from "@/state";
import { CameraPreview } from "../camera-preview";
import { MdFilledSelect as mdFilledSelect } from "@material/web/select/filled-select";
import { useMediaDevices } from "@/hooks/useMediaDevices";
import { useVideoStream } from "@/hooks/useVideoStream";

export function InstructionCamera({ tr }: { tr: any }) {
  const { videoStream, startCapturing } = useVideoStream();

  const sRef = React.useRef<mdFilledSelect>(null);
  const { videoDevices, videoDeviceId } = useMediaDevices();

  async function assignCamera(deviceId?: string) {
    appState.media.videoDeviceId = deviceId ?? appState.media.videoDeviceId;
    if (appState.media.videoDeviceId) {
      sRef.current?.select(appState.media.videoDeviceId);
    }
  }

  React.useEffect(() => {
    assignCamera(videoDevices?.[0]?.deviceId);
  }, [videoDevices]);

  React.useEffect(() => {
    startCapturing();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <p className="text-start">{tr("inst.i2")}</p>
      <div>
        <MdFilledSelect
          ref={sRef}
          value={videoDeviceId}
          onInput={(e) => {
            const select = e.target as mdFilledSelect;
            assignCamera(select.value);
          }}
        >
          {videoDevices?.map((d, i) => (
            <MdSelectOption key={i} value={d.deviceId}>
              <div slot="headline">{d.label}</div>
            </MdSelectOption>
          ))}
        </MdFilledSelect>
      </div>
      <CameraPreview videoStream={videoStream} />
    </div>
  );
}
