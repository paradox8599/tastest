import { MdCircularProgress } from "./md";

export function Loading() {
  return (
    <div className="flex-center">
      <MdCircularProgress indeterminate></MdCircularProgress>
    </div>
  );
}
