export const en = {
  locale: "English",
  components: {
    buttons: {
      logout: "Logout",
    },
  },
  indexPage: {
    buttons: {
      start: "Start",
    },
  },
  positionInstructionPage: {
    buttons: {
      skip: "Skip",
      continue: "Continue",
      replay: "Replay",
    },
  },
  consentPage: {
    instruction: "Please complete your details below to confirm consent",
    buttons: {
      viewConsentForm: "View Consent Form",
      submit: "Submit",
      skip: "Skip",
    },
    form: {
      name: "Name",
      firstName: "First Name",
      lastName: "Last Name",
      dob: "Date of birth",
      gender: "Gender",
      female: "Female",
      male: "Male",
      other: "Other",
      unknown: "Prefer Not To Say",
      agree: "I DO consent to participate in the TapTalk project",
      disagree: "I DO NOT consent to participate in the TapTalk project",
      dobWarn: "You have to be aged 18 or over to participate Tap Talk",
      consentWarn:
        "You must consent to participate in the TapTalk project to continue.",
      consentSubmitError:
        "Something went wrong, please check your input details and try again.",
    },
  },
  preparePage: {
    buttons: {
      prev: "Previous",
      next: "Next",
    },
    title: "System Test",
    instructions: "Instructions",
    camera: "Camera",
    microphone: "Microphone",
    cameraLowercase: "camera",
    microphoneLowercase: "microphone",
    inst: {
      i1: "Please sit arm's length from your screen and make sure the room is brightly lit.",
      i2: "Can you see your image? If not, switch on the camera using the drop down menu below.",
      i3: 'Speak out loudly, saying "1, 2, 3, 4, 5". Can you see the coloured waves moving in the box? If not, switch on the microphone using the drop down menu below.',
    },
    errorMessages: {
      permission: "Please allow {device} access on your borwser to proceed.",
    },
    dialog: {
      device: {
        title: "Device not available",
        content: "Please select your {device} device and try again.",
        confirm: "Ok",
      },
      wellDone: {
        title: "Well Done!",
        content: "You have completed the {device} setting.",
        confirm: "Next",
      },
    },
  },
  questionnairePage: {
    buttons: {
      skip: "Skip",
      yes: "YES",
      next: "NEXT >",
    },
    question: {
      unsupport: "Not Support",
    },
    start: {
      i1: "Thank you for completing <tap>Tap</tap> <talk>Talk</talk>!",
      i2: 'We would really value your feedback on this newtest. Please click "YES" below to completethe one-minute questionnaire',
    },
    end: {
      content:
        "Thank you once again for your\ntime and effort comleting TAS\nTest. We will use your feedback to\nfurther improve the test",
    },
    complete: {
      content: "Thank you\nfor completing\n<tap>Tap</tap> <talk>Talk</talk>",
      logout: "LOGOUT",
    },
    form: {
      submitError:
        "Something went wrong, please check your input details and try again.",
      unexpected: "Unexpected error occurred",
    },
  },
  directionsPage: {
    title: "There are {total} tests\nin total",
    content: "Each test takes 10 - 20 seconds.",
    buttons: {
      next: "NEXT >",
    },
  },
  countDownPage: {
    title: "Tap big and fast",
    content: "Get ready to start\ntapping\nafter {time} seconds",
  },
  wellDonePage: {
    wellDone: {
      title: "Well Done!",
      content: "You completed the\nrecording.",
    },
    next: {
      content: "I completed the test",
      label: "NEXT >",
    },
    again: {
      content: "Make a mistake?",
      label: "TRY AGAIN",
    },
  },
  testsPage: {
    buttons: {
      skip: "Skip",
      next: "Next",
      tryAgain: "Try Again",
    },
    info: {
      content:
        "There are {total} tests in total.\nEach test takes 10 - 20 seconds.",
    },
    getReady: {
      cameraText: "Get ready to start tapping in {seconds} seconds",
      audioText: "Get Ready to repeat the sound in {seconds} seconds",
    },
    toast: {
      uploadError: "Cannot upload test result, please try again",
    },
  },
};

export type Tr = typeof en;
