import fs from "fs";
import { en } from "./tr-en";
import { zh } from "./tr-zh";
import { de } from "./tr-de";
fs.mkdirSync("messages/output", { recursive: true });
fs.writeFileSync("messages/output/en.json", JSON.stringify(en, null, 2));
fs.writeFileSync("messages/output/zh.json", JSON.stringify(zh, null, 2));
fs.writeFileSync("messages/output/de.json", JSON.stringify(de, null, 2));
console.log("Exported messages to json files");
