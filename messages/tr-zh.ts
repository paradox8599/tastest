import { Tr } from "./tr-en";

export const zh: Tr = {
  locale: "中文",
  components: {
    buttons: {
      logout: "退出",
    },
  },
  indexPage: {
    buttons: {
      start: "开始",
    },
  },
  positionInstructionPage: {
    buttons: {
      skip: "跳过",
      continue: "继续",
      replay: "重新播放",
    },
  },
  consentPage: {
    instruction: "请填写以下信息以确认同意",
    buttons: {
      viewConsentForm: "查看隐私政策",
      submit: "提交",
      skip: "跳过",
    },
    form: {
      name: "姓名",
      firstName: "名字",
      lastName: "姓氏",
      dob: "出生日期",
      gender: "性别",
      female: "女",
      male: "男",
      other: "其他",
      unknown: "不透露",
      agree: "同意在 TapTalk 项目中参与测试",
      disagree: "不同意在 TapTalk 项目中参与测试",
      dobWarn: "您必须满18周岁才能参与测试",
      consentWarn: "您必须同意在TapTalk项目中参与测试才能继续。",
      consentSubmitError:
        "Something went wrong, please check your input details and try again.",
    },
  },
  preparePage: {
    buttons: {
      prev: "上一步",
      next: "下一步",
    },
    title: "系统测试",
    instructions: "说明",
    camera: "摄像头",
    microphone: "麦克风",
    cameraLowercase: "摄像头",
    microphoneLowercase: "麦克风",
    inst: {
      i1: "请将屏幕放至约一臂远，并确保房间明亮。",
      i2: "可以看到您的图像吗？如果没有，请使用下方下拉菜单选择摄像头设备。",
      i3: '请大声说出 "1, 2, 3, 4, 5". 您能看到下方框中的音量波动吗? 如果没有，请使用下方下拉菜单选择麦克风设备.',
    },
    errorMessages: {
      permission: "请允许浏览器的{device}权限来继续",
    },
    dialog: {
      device: {
        title: "Device not available",
        content: "请选择您的{device}并再次尝试。",
        confirm: "好的",
      },
      wellDone: {
        title: "干得漂亮！",
        content: "你已完成{device}设置。",
        confirm: "下一步",
      },
    },
  },
  questionnairePage: {
    buttons: {
      skip: "跳过",
      yes: "确定",
      next: "下一步 >",
    },
    question: {
      unsupport: "暂不支持",
    },
    start: {
      i1: "感谢您完成 <tap>Tap</tap> <talk>Talk</talk>！",
      i2: "我们非常重视您对这个新测试的反馈。请点击下面的“确定”来完成一分钟的调查问卷",
    },
    end: {
      content:
        "再次感谢您\n花费时间和精力完成 TAS\n测试。我们将利用您的反馈\n来进一步改进测试",
    },
    complete: {
      content: "感谢您\n完成\n<tap>Tap</tap> <talk>Talk</talk>",
      logout: "退出",
    },
    form: {
      submitError: "出现问题，请检查您的输入信息，然后重试。",
      unexpected: "发生意外错误",
    },
  },
  directionsPage: {
    title: "总共有7个\n测试",
    content: "每次测试需要 10 - 20 秒",
    buttons: {
      next: "下一步 >",
    },
  },
  countDownPage: {
    title: "快速大力点击",
    content: "准备在 {time} 秒后\n开始点击",
  },
  wellDonePage: {
    wellDone: {
      title: "干得漂亮",
      content: "您已完成\n录制",
    },
    next: {
      content: "我完成了测试",
      label: "下一步 >",
    },
    again: {
      content: "犯了错误？",
      label: "再试一次",
    },
  },
  testsPage: {
    buttons: {
      skip: "跳过",
      next: "下一步",
      tryAgain: "再试一次",
    },
    info: {
      content:
        "There are {total} tests in total.\nEach test takes 10 - 20 seconds.",
    },
    getReady: {
      cameraText: "Get ready to start tapping in {seconds} seconds",
      audioText: "Get Ready to repeat the sound in {seconds} seconds",
    },
    toast: {
      uploadError: "Cannot upload test result, please try again",
    },
  },
};
