import type { Config } from "tailwindcss";
import { MD } from "./src/style";

const config: Config = {
  content: ["./src/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      screens: {
        // follow chrome's breakpoints
        sm: "320px",
        smd: "375px",
        md: "425px",
        lg: "768px",
        xl: "1024px",
      },
      colors: MD,
      animation: {
        "border-jump": "border-jump 1s ease-in-out 2",
      },
      keyframes: {
        "border-jump": {
          "0%, 100%": { transform: "scale(1)" },
          "50%": { transform: "scale(1.1)" },
        },
      },
    },
  },

  plugins: [],
};
export default config;
