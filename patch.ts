import fs from "fs";

// react-voice-visualizer
// supress errors when navigating away from pages that contain voice visualizer component
const paths = [
  "node_modules/react-voice-visualizer/dist/react-voice-visualizer.js",
  "node_modules/react-voice-visualizer/dist/react-voice-visualizer.umd.cjs",
];
for (const path of paths) {
  const fileBuffer = fs.readFileSync(path);
  const replaced = fileBuffer
    .toString()
    .replaceAll(
      "q.current.getByteTimeDomainData",
      "q.current?.getByteTimeDomainData",
    );
  fs.writeFileSync(path, replaced);
}
