# TAS Test

## Getting Started

- Install Node.js 18 and [Bun](https://bun.sh/)
- Edit `.env.local` to override the defaults.
- Install dependencies: `bun i` or `bun install`
- Run the development server: `bun dev` or `bun run dev`

## Page Order

1. `/`: Start Page
2. `/position-instruction`: instruction video
3. `/consent`: Consent form
4. `/prepare`: Camera & Microphone setup
5. `/tests`: Instructions and Tests
6. `/questionnaire`: Questionnaire for user feedback

## Translations(i18n)

Edit `messages/tr-{locale}.ts` to add new translations. Type of all other locale settings are based on English settings in [messages/tr-en.ts](messages/tr-en.ts).

## Icons

Currently using `rounded` [Material Icons](https://fonts.google.com/icons).

### To change

e.g. change from `rounded` to `outlined`, or change `filled`, `weight`, etc.

- Update `@import`s in [src/app/globals.css](src/app/globals.css)
- Update `.material-symbols-rounded` in [src/app/globals.css](src/app/globals.css)
- Update `className` for `<span>` in [src/components/icon.tsx](src/components/icon.tsx).

### Usage

On [Material Icons](https://fonts.google.com/icons) page , search for the icon you want to use and select it, then copy the text inside `span` tag from `Inserting the icon` section on the right side.

Add with `I` component, tailwind can be used to change text styles.

```tsx
import I from "@/components/icon";
```

```tsx
<I className="text-sm text-green-500">home</I>
```

## UI Components

Using [@material/web](https://github.com/material-components/material-web), wrapped with [@lit/react](https://lit.dev/docs/frameworks/react/).

Edit wrappers in [src/components/md.tsx](src/components/md.tsx).

## Theme & Color

Using [Material Color Scheme](https://material-web.dev/theming/color/).

Use [Material Theme Builder](https://material-foundation.github.io/material-theme-builder/) as reference.

See more details in [src/app/globals.css](./src/app/globals.css), [tailwind.config.ts](./tailwind.config.ts).

## In App Notifications

Using [react-toastify](https://fkhadra.github.io/react-toastify).

`<ToastContainer />` configs has been added to [src/providers/app-message-provider.tsx](src/providers/app-message-provider.tsx), call `toast` from anywhere in the app.

## Other Notes

- [Build an app with Next.js and Bun](https://bun.sh/guides/ecosystem/nextjs)
- Commit message must follow the [rules](https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional)
  - labels: `build` `chore` `ci` `docs` `feat` `fix` `perf` `refactor` `revert` `style` `test`
- In env variables, URLs must have an trailing slash `/`.
- To access media device on iOS, https is required

## Deploy

- Deploy to [Vercel](https://vercel.com/)
- Deploy to [Fly.io](https://fly.io/)
- Deploy to own server with [Docker](https://www.docker.com/)

## Dependencies

- [@lit/react](https://lit.dev/docs/frameworks/react) - React wrapper for lit components
- [@material/web](https://github.com/material-components/material-web) - Material Design Web Components
- [date-fns](https://date-fns.org/) - Date formatting, also used by react-day-picker (may be used in the future for desktop)
- [Formik](https://formik.org/docs) - Form validation
- [Next.js](https://nextjs.org/) - React framework
- [next-intl](https://next-intl-docs.vercel.app/docs) - i18n
- [react-toastify](https://fkhadra.github.io/react-toastify) - Toast notifications
- [react-voice-visualizer](https://github.com/YZarytskyi/react-voice-visualizer) - Voice visualizer
- [Sharp](https://github.com/lovell/sharp) - Image processing used by Next.js (You can ignore it)
- [SWR](https://swr.vercel.app/) - Data fetching
- [Valtio](https://github.com/pmndrs/valtio) - State management
- [TailwindCSS](https://tailwindcss.com/) - CSS framework
